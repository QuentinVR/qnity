*Engine hierarchy

SceneManager - > Scene -> GameObject -> BaseObject -> Custom Objects

*Scene holds a map that tracks platforms and walls

*items with a rigidbody can be moved and automatically handle collision with the map

*Colliders can be used to objects who intent to interact with rigidbodies

*InputComponents can be used to control Actors

*Both player input (controller and keyboard) as Agent input is supported and
transparent to the actor

*Textures are loaded only once and distributed on demand

*Use of smart pointers to prevent dangling pointers, 
any raw pointers are not owned by the object and should not be deleted

*Logger logs warnings/errors to a file, on program exit, the debug window will show if errors occured

