#include "MoveCommand.h"
#include <iostream>
#include <windows.h>


MoveCommand::MoveCommand(Qnity::GameObject* owner, float movespeed)
	: Command{owner}
	, m_MoveSpeed{ movespeed }
{

} 

void MoveCommand::Execute()
{
	m_pRb.lock()->SetXVelocity(m_MoveSpeed);
}

void MoveCommand::Initialize()
{
	m_pRb = m_pOwner->GetComponent<Qnity::RigidBodyComponent>();
}