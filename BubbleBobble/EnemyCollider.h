#pragma once
#include "ColliderComponent.h"

enum class EnemyTrapped;

class EnemyCollider : public Qnity::ColliderComponent
{
public:
	EnemyCollider(std::shared_ptr<Qnity::Map> map);
	virtual ~EnemyCollider();

	EnemyCollider(const EnemyCollider&) = delete;
	EnemyCollider(EnemyCollider&& other) = delete;
	EnemyCollider& operator=(const EnemyCollider& other) = delete;
	EnemyCollider& operator=(EnemyCollider&& other) = delete;

	virtual void Update(int MsPerFrame) override;
	virtual void LateUpdate(int MsPerFrame) override;
	virtual void Render() const override;
	virtual void Initialize() override;

	virtual void HitEvent(Qnity::GameObject* other, bool terrain);

private:

};
