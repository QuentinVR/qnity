#include "QnityPCH.h"
#include "ZenChanBehavior.h"

ZenChanBehavior::ZenChanBehavior()
	: ActorBehavior{}
{

}

ZenChanBehavior::~ZenChanBehavior()
{

}

void ZenChanBehavior::Initialize()
{
	ActorBehavior::Initialize();

	//init sprite
	m_pTextureComponent.lock()->SetSpriteNumber(60);
	m_pTextureComponent.lock()->SetTextureNumber(1);
	m_SpriteOffset = 60;
}

void ZenChanBehavior::Update(int MsPerFrame)
{
	ActorBehavior::Update(MsPerFrame);
}

void ZenChanBehavior::LateUpdate(int MsPerFrame)
{
	ActorBehavior::LateUpdate(MsPerFrame);
}

void ZenChanBehavior::Render() const
{

}

void ZenChanBehavior::SetState(Qnity::ActorState state)
{
	Qnity::ActorBehavior::SetState(state);
}
