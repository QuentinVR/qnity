#include "ChargeCommand.h"
#include <iostream>
#include <windows.h>
#include "ActorBehavior.h"

ChargeCommand::ChargeCommand(Qnity::GameObject* owner)
	: Command{ owner }
{

}

void ChargeCommand::Execute()
{
	m_pRb.lock()->SetBoost(64, 2.f);
}

void ChargeCommand::Initialize()
{
	m_pRb = m_pOwner->GetComponent<Qnity::RigidBodyComponent>();
}