#pragma once
#include "AgentInputComponent.h"
#include "JumpCommand.h"
#include "MoveCommand.h"

enum class ZenChanCommands
{
	MoveLeft, MoveRight, Jump, Charge
};

class Qnity::RigidBodyComponent;

class ZenChanInput final : public Qnity::AgentInputComponent
{
public:
	ZenChanInput(std::weak_ptr<Qnity::GameObject> player1, std::weak_ptr<Qnity::GameObject> player2);
	virtual ~ZenChanInput();

	ZenChanInput(const ZenChanInput& other) = delete;
	ZenChanInput(ZenChanInput&& other) = delete;
	ZenChanInput& operator=(const ZenChanInput& other) = delete;
	ZenChanInput& operator=(ZenChanInput&& other) = delete;

	void Update(int MsPerFrame) override;
	void LateUpdate(int MsPerFrame) override;
	void Render() const override;
	void Initialize() override;

private:
	std::weak_ptr<Qnity::RigidBodyComponent> m_pRigidBody;
	bool m_GoRight;

	int m_JumpTimer;
	int m_JumpCooldown;

	bool m_Player2Present;
};
