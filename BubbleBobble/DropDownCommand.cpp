#include "DropDownCommand.h"
#include <iostream>
#include <windows.h>
#include "ActorBehavior.h"

DropDownCommand::DropDownCommand(Qnity::GameObject* owner)
	: Command{ owner }
{

}

void DropDownCommand::Execute()
{
	m_pActor.lock()->SetState(Qnity::ActorState::dropDown);
}

void DropDownCommand::Initialize()
{

}