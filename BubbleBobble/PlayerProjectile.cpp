#include "PlayerProjectile.h"
#include "Bubble.h"
#include <AgentInputComponent.h>
#include <Logger.h>
#include "SubjectComponent.h"


PlayerProjectile::PlayerProjectile(Qnity::Vector2<float> velocity, std::shared_ptr<Qnity::Map> map, std::weak_ptr<Qnity::GameObject> player1, std::weak_ptr<Qnity::GameObject> player2)
	: ProjectileCollider{velocity,  Qnity::ColliderTarget::badGuys, map}
	, m_pPlayer1{player1}
	, m_pPlayer2{player2}
{

}

PlayerProjectile::~PlayerProjectile()
{

}

void PlayerProjectile::HitEvent(Qnity::GameObject* other, bool terrain)
{
	ProjectileCollider::HitEvent(other, terrain);

	if (terrain)
	{
		CreateBubble(EnemyTrapped::none);
	}
	else
	{
		other->SetDestroy();

		std::weak_ptr<Qnity::AgentInputComponent> agent = other->GetComponent<Qnity::AgentInputComponent>();

		if (agent.expired())
		{
			Qnity::Logger().LogError("Wrong hit event created");
			return;
		}

		AgentType type{ agent.lock()->GetAgentType() };

		switch (type)
		{
		case AgentType::ZenChan:
			CreateBubble(EnemyTrapped::zenchang);
			break;
		case AgentType::Maita:
			CreateBubble(EnemyTrapped::maita);
			break;
		default:
			break;
		}
	}
}

void PlayerProjectile::CreateBubble(EnemyTrapped enemy)
{
	//create bubble
	std::shared_ptr<Qnity::GameObject> object = std::make_shared<Qnity::GameObject>();
	std::shared_ptr<Qnity::RigidBodyComponent> rb = std::make_shared<Qnity::RigidBodyComponent>(m_pMap, 48.f);
	std::shared_ptr<Qnity::SubjectComponent> subject = std::make_shared<Qnity::SubjectComponent>();
	rb->SetGravity(false);
	std::shared_ptr<Bubble> collider = std::make_shared<Bubble>(m_pMap, enemy, m_pPlayer1, m_pPlayer2);
	std::shared_ptr<Qnity::Texture2DComponent> texture = std::make_shared<Qnity::Texture2DComponent>(3, true);
	texture->SetDisplaySize(48, 48);

	object->AddComponent(rb);
	object->AddComponent(collider);
	object->AddComponent(texture);
	object->AddComponent(subject);

	object->SetPosition(m_pOwner->GetTransform().GetPosition());

	m_pMap->MarkForAdd(object);
}
