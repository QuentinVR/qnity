#include "ShootCommand.h"
#include <RigidBodyComponent.h>
#include "ActorBehavior.h"
#include "PlayerProjectile.h"

ShootCommand::ShootCommand(Qnity::GameObject* owner, std::shared_ptr<Qnity::Map> map, int playerNumber, std::weak_ptr<Qnity::GameObject> player1, std::weak_ptr<Qnity::GameObject> player2)
	: Command{ owner }
	, m_pMap{ map }
	, m_PlayerNumber{ playerNumber }
	, m_pPlayer1{player1}
	, m_pPlayer2{player2}
{

}

void ShootCommand::Execute()
{
	if (!m_pPlayerBehavior.lock()->CanShoot())
	{
		return;
	}
	
	//set state
	m_pActor.lock()->SetState(Qnity::ActorState::attacking);

	//create bullet from scratch
   	std::shared_ptr<Qnity::GameObject> object = std::make_shared<Qnity::GameObject>();
	std::shared_ptr<Qnity::RigidBodyComponent> rb = std::make_shared<Qnity::RigidBodyComponent>(m_pMap, 64.f);
	rb->SetColliderGroup(Qnity::ColliderTarget::goodGuys);
	rb->SetGravity(false);

	bool dirIsRight = m_pActor.lock()->IsLookingRight();
	object->SetPosition(m_pActor.lock()->GetPos(false));
	
	float speed{1024.f};
	if (!dirIsRight)
	{
		speed *= -1;
	}

	std::shared_ptr<PlayerProjectile> collider = std::make_shared<PlayerProjectile>(Qnity::Vector2<float>{speed, 0.f}, m_pMap, m_pPlayer1, m_pPlayer2);
	std::shared_ptr<Qnity::Texture2DComponent> texture = std::make_shared<Qnity::Texture2DComponent>(2, true);
	
	if (m_PlayerNumber == 0)
	{
		texture->SetSpriteNumber(180);
		collider->SetSpriteOffset(180);
	}
	else
	{
		texture->SetSpriteNumber(195);
		collider->SetSpriteOffset(195);
	}

	texture->SetDisplaySize(48, 48);
	collider->SetAnimationFrames(8);


	object->AddComponent(rb);
	object->AddComponent(collider);
	object->AddComponent(texture);

	//add projectile to map
	m_pMap->MarkForAdd(object);
}

void ShootCommand::Initialize()
{
	m_pPlayerBehavior = m_pOwner->GetComponent<PlayerBehavior>();
}