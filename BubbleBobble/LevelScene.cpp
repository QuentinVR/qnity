#include "QnityPCH.h"
#include "LevelScene.h"
#include "PlayerInputComponent.h"
#include "JumpCommand.h"
#include "MoveCommand.h"
#include "DropDownCommand.h"
#include "ShootCommand.h"
#include "FPSCounter.h"
#include <Texture2DComponent.h>
#include "RigidBodyComponent.h"
#include "PlayerBehavior.h"
#include "TextureBank.h"
#include "ZenChanInput.h"
#include "ZenChanBehavior.h"
#include <ColliderComponent.h>
#include "EnemyCollider.h"
#include "levelObserver.h"
#include <SubjectComponent.h>
#include "ResourceManager.h"
#include <functional>

bool LevelScene::m_TexturesLoaded = false;

LevelScene::LevelScene(const std::string& name, int levelNumber, int numberOfPlayers)
	: Scene{ name }
	, m_LevelNumber{levelNumber}
	, m_NumberOfEnemies{3 + levelNumber / 2}
	, m_NumberOfPlayers{ numberOfPlayers }
	, m_Score{}
	, m_pPlayerCharacter1{nullptr}
	, m_pPlayerCharacter2{nullptr}
	, m_NextSceneThread{}

{
	LoadTextures();

	LoadMap();
}

LevelScene::~LevelScene()
{
	if (m_NextSceneThread.joinable())
	{
		m_NextSceneThread.detach();
	}
}

void LevelScene::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void LevelScene::Render() const
{
	m_pMap->Render();
}

void LevelScene::LateUpdate(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void LevelScene::Initialize()
{
	using namespace Qnity;

	//create osbserver
	m_Observer = std::make_shared<LevelObserver>(this);

	//create score text component
	auto font = Qnity::ResourceManager::GetInstance().LoadFont("../Resources/Lingua.otf", 36);
	std::shared_ptr<GameObject> textObject = std::make_shared<GameObject>();
	m_TextComponent = std::make_shared<Qnity::TextComponent>(std::to_string(m_Score), font);
	textObject->AddComponent(m_TextComponent);
	textObject->SetPosition(512.f, 100.f);
	Add(textObject);

	//try and recycle characters
	AddPlayers(m_NumberOfPlayers);
	
	for (int i{}; i < m_NumberOfEnemies; i++)
	{
		AddZenChan(Qnity::Vector2<float>{(i % 5) * 64.f - 192.f, (i/5) * 20.f});
	}

	//create next scene
	m_NextSceneThread = std::thread(&LevelScene::CreateNextLevel, this);
}

void LevelScene::AddPlayers(int number)
{
	using namespace Qnity;

	std::shared_ptr<GameObject> player1 = std::make_shared<GameObject>();

	std::shared_ptr<PlayerInputComponent> pInput = std::make_shared<PlayerInputComponent>(0);
	player1->AddComponent(pInput);

	std::shared_ptr<Texture2DComponent> pTexture = std::make_shared<Texture2DComponent>(int(TextureNames::sprites0), true);
	pTexture->SetSpriteNumber(0);
	pTexture->SetDisplaySize(64, 64);
	player1->AddComponent(pTexture);
	player1->SetPosition(100.f, 500.f);

	std::shared_ptr<Qnity::SubjectComponent> subject = std::make_shared<Qnity::SubjectComponent>();
	player1->AddComponent(subject);

	std::shared_ptr<RigidBodyComponent> rb = std::make_shared<RigidBodyComponent>(m_pMap);
	player1->AddComponent(rb);
	rb->SetColliderGroup(ColliderTarget::goodGuys);

	std::shared_ptr<GameObject> player2{nullptr};

	if (number > 1)
	{
		player2 = std::make_shared<GameObject>();

		std::shared_ptr<PlayerInputComponent> pInput2 = std::make_shared<PlayerInputComponent>(1);
		player2->AddComponent(pInput2);

		std::shared_ptr<Texture2DComponent> pTexture2 = std::make_shared<Texture2DComponent>(int(TextureNames::sprites0), true);
		pTexture2->SetSpriteNumber(30);
		pTexture2->SetDisplaySize(64, 64);
		player2->AddComponent(pTexture2);
		player2->SetPosition(400.f, 500.f);

		std::shared_ptr<Qnity::SubjectComponent> subject2 = std::make_shared<Qnity::SubjectComponent>();
		player2->AddComponent(subject2);

		std::shared_ptr<RigidBodyComponent> rb2 = std::make_shared<RigidBodyComponent>(m_pMap);
		player2->AddComponent(rb2);
		rb2->SetColliderGroup(ColliderTarget::goodGuys);

		pInput2->RegisterInput(new InputAction{ XINPUT_GAMEPAD_A, 'W', InputType::PRESSED, new JumpCommand{player2.get()} });
		pInput2->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_LEFT, 'A', InputType::HOLD, new MoveCommand{player2.get(), -256.f} });
		pInput2->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_RIGHT, 'D', InputType::HOLD, new MoveCommand{player2.get(), 256} });
		pInput2->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_DOWN, 'S', InputType::HOLD, new DropDownCommand{player2.get()} });
		pInput2->RegisterInput(new InputAction{ XINPUT_GAMEPAD_X, 'X', InputType::PRESSED, new ShootCommand{player2.get(), m_pMap, 1, player1, player2} });

		std::shared_ptr<PlayerBehavior> playerBehavior2 = std::make_shared<PlayerBehavior>(1);
		player2->AddComponent(playerBehavior2);
	}

	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_A, VK_UP, InputType::PRESSED, new JumpCommand{player1.get()} });
	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_LEFT, VK_LEFT, InputType::HOLD, new MoveCommand{player1.get(), -256.f} });
	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_RIGHT, VK_RIGHT, InputType::HOLD, new MoveCommand{player1.get(), 256} });
	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_DOWN, VK_DOWN, InputType::HOLD, new DropDownCommand{player1.get()} });
	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_X, VK_SPACE, InputType::PRESSED, new ShootCommand{player1.get(), m_pMap, 0, player1, player2} });

	std::shared_ptr<PlayerBehavior> playerBehavior = std::make_shared<PlayerBehavior>(0);
	player1->AddComponent(playerBehavior);
	Add(player1);

	m_pPlayerCharacter1 = player1;

	if (number > 1)
	{
		m_pPlayerCharacter2 = player2;
		Add(player2);
	}
}

void LevelScene::AddZenChan(Qnity::Vector2<float> offset)
{
	using namespace Qnity;
	//spawn zen chan
	std::shared_ptr<GameObject> zenchanObject = std::make_shared<GameObject>();
	std::shared_ptr<Texture2DComponent> pTextureZenChan = std::make_shared<Texture2DComponent>(int(TextureNames::sprites0), true);
	pTextureZenChan->SetDisplaySize(64, 64);
	std::shared_ptr<ZenChanBehavior> pZenChanBehavior = std::make_shared<ZenChanBehavior>();
	std::shared_ptr<ZenChanInput> pZenChanInput = std::make_shared<ZenChanInput>(m_pPlayerCharacter1, m_pPlayerCharacter2);
	std::shared_ptr<RigidBodyComponent> rbZenChan = std::make_shared<RigidBodyComponent>(m_pMap);
	rbZenChan->SetColliderGroup(Qnity::ColliderTarget::badGuys);
	std::shared_ptr<EnemyCollider> pEnemyCollider = std::make_shared<EnemyCollider>(m_pMap);

	zenchanObject->SetPosition(400.f + offset.x, 64.f + offset.y);
	zenchanObject->AddComponent(pTextureZenChan);
	zenchanObject->AddComponent(pZenChanBehavior);
	zenchanObject->AddComponent(pZenChanInput);
	zenchanObject->AddComponent(rbZenChan);
	zenchanObject->AddComponent(pEnemyCollider);

	Add(zenchanObject);
}

void LevelScene::LoadMap()
{
	m_pMap = std::make_shared<Qnity::Map>(this);
	m_pMap->LoadTexture(int(TextureNames::blocks), m_LevelNumber);
	m_pMap->LoadTiles("../Resources/leveldata.dat", m_LevelNumber);
}

int LevelScene::GetLevelNumber()
{
	return m_LevelNumber;
}

std::shared_ptr<Qnity::GameObject> LevelScene::GetPlayer(int number)
{
	if (number == 0)
	{
		return m_pPlayerCharacter1;
	}
	else
	{
		return m_pPlayerCharacter2;
	}
}

void LevelScene::DecreaseEnemies()
{
	m_NumberOfEnemies--;

	AddScore(1000);

	if (m_NumberOfEnemies == 0)
	{
		m_NextSceneThread.join();
		
		m_NextLevel->SetScore(m_Score);
		m_NextLevel->SetPlayer(m_pPlayerCharacter1, 0);
		m_NextLevel->SetPlayer(m_pPlayerCharacter2, 1);

		Qnity::SceneManager::GetInstance().AddScene(m_NextLevel);
		Qnity::SceneManager::GetInstance().GoToNextLevel();
	}
}

void LevelScene::AddScore(int score)
{
	m_Score += score;

	m_TextComponent->SetText(std::to_string(m_Score));
}

void LevelScene::GameOver()
{
	m_TextComponent->SetText("Game Over");
}


void LevelScene::Add(const std::shared_ptr<Qnity::GameObject>& object)
{
	Scene::Add(object);

	//check if subject
	std::weak_ptr<Qnity::SubjectComponent> subject = object->GetComponent<Qnity::SubjectComponent>();
	if (subject.lock() != nullptr)
	{
		subject.lock()->AddObserver(m_Observer);
	}
}

int LevelScene::GetScore()
{
	return m_Score;
}

void LevelScene::CreateNextLevel()
{
	m_NextLevel = std::make_shared<LevelScene>("funlevel", m_LevelNumber + 1, m_NumberOfPlayers);
}

void LevelScene::LoadTextures()
{
	if (!m_TexturesLoaded)
	{
		//load all textures
		Qnity::TextureBank::GetInstance().SetResourcePath("../Resources/");
		Qnity::TextureBank::GetInstance().AddSDLTexture("blocks.png");
		Qnity::TextureBank::GetInstance().AddSDLTexture("sprites0.png");
		Qnity::TextureBank::GetInstance().AddSDLTexture("sprites1.png");
		Qnity::TextureBank::GetInstance().AddSDLTexture("sprites2.png");
		Qnity::TextureBank::GetInstance().AddSDLTexture("sprites3.png");
		Qnity::TextureBank::GetInstance().AddSDLTexture("sprites4.png");
		Qnity::TextureBank::GetInstance().AddSDLTexture("sprites5.png");

		m_TexturesLoaded = true;
	}
}

void LevelScene::SetPlayer(std::shared_ptr<Qnity::GameObject> player, int number)
{
	if (number == 0)
	{
		m_pPlayerCharacter1 = player;
	}
	else
	{
		m_pPlayerCharacter2 = player;
	}
}

void LevelScene::SetScore(int score)
{
	m_Score = score;
}