#pragma once
#include "Command.h"
#include "RigidBodyComponent.h"

class MoveCommand : public Qnity::Command
{
public:
	MoveCommand(Qnity::GameObject* owner, float movespeed);
	virtual ~MoveCommand() = default;
	void Execute() override;
	void Initialize() override;

private:
	std::weak_ptr<Qnity::RigidBodyComponent> m_pRb;
	float m_MoveSpeed;;
};
