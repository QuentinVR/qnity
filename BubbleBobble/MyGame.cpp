#include "MyGame.h"
#include <InputManager.h>
#include "JumpCommand.h"
#include "SceneManager.h"
#include "Texture2DComponent.h"
#include "TextComponent.h"
#include "GameObject.h"
#include "Scene.h"
#include "FPSCounter.h"
#include "ResourceManager.h"
#include "LevelScene.h"

void Qnity::MyGame::LoadGame() const
{

	SceneManager::GetInstance().AddScene(std::make_shared<LevelScene>("TestScene", 0, 2));

}