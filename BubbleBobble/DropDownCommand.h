#pragma once
#include "Command.h"

class DropDownCommand : public Qnity::Command
{
public:
	DropDownCommand(Qnity::GameObject* owner);
	virtual ~DropDownCommand() = default;
	void Execute() override;
	void Initialize() override;

private:

};
