#pragma once
#include "ColliderComponent.h"
#include <SubjectComponent.h>

enum class EnemyTrapped;

class FoodCollider : public Qnity::ColliderComponent
{
public:
	FoodCollider(std::shared_ptr<Qnity::Map> map, EnemyTrapped type);
	virtual ~FoodCollider();

	FoodCollider(const FoodCollider&) = delete;
	FoodCollider(FoodCollider&& other) = delete;
	FoodCollider& operator=(const FoodCollider& other) = delete;
	FoodCollider& operator=(FoodCollider&& other) = delete;

	virtual void Update(int MsPerFrame) override;
	virtual void LateUpdate(int MsPerFrame) override;
	virtual void Render() const override;
	virtual void Initialize() override;

	virtual void HitEvent(Qnity::GameObject* other, bool terrain);

private:
	EnemyTrapped m_Type;
	int m_LifeTime;
	int m_TimeBeforePickup;

	bool m_HasHitGround;

	Qnity::Vector2<float> m_Velocity;

	std::weak_ptr<Qnity::SubjectComponent> m_Subject;

};
