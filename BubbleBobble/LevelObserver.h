#pragma once
#include "QnityPCH.h"
#include "Observer.h"

class LevelScene;

enum class Event
{
	SCORED_POINTS,
	ENEMY_KILLED,
	GAME_OVER
};

class LevelObserver : public Qnity::Observer
{
public:
	LevelObserver(LevelScene* scene);
	virtual ~LevelObserver() = default;
	void OnNotify(Qnity::Event event, int value) override;

private:
	LevelScene* m_Scene;

};