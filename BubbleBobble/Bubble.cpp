#include "QnityPCH.h"
#include "Bubble.h"
#include "ZenChanBehavior.h"
#include "ZenChanInput.h"
#include "FoodCollider.h"
#include "EnemyCollider.h"

Bubble::Bubble(std::shared_ptr<Qnity::Map> map, EnemyTrapped enemy, std::weak_ptr<Qnity::GameObject> player1, std::weak_ptr<Qnity::GameObject> player2)
	: ProjectileCollider{ Qnity::Vector2<float>{0.f, -90.f}, Qnity::ColliderTarget::goodGuys, map }
	, m_Trapped{enemy}
	, m_pPlayer1{player1}
	, m_pPlayer2{player2}
{

}

Bubble::~Bubble()
{

}

void Bubble::Initialize()
{
	ProjectileCollider::Initialize();

	m_LifeTime = 2500;

	switch (m_Trapped)
	{
	case EnemyTrapped::none:
		m_pTextureComponent.lock()->SetTextureNumber(3);
		m_pTextureComponent.lock()->SetSpriteNumber(90);
		m_SpriteOffset = 90;
		break;
	case EnemyTrapped::zenchang:
		m_pTextureComponent.lock()->SetTextureNumber(2);
		m_pTextureComponent.lock()->SetSpriteNumber(210);
		m_SpriteOffset = 210;
		break;
	case EnemyTrapped::maita:
		m_pTextureComponent.lock()->SetTextureNumber(3);
		m_pTextureComponent.lock()->SetSpriteNumber(60);
		m_SpriteOffset = 60;
		break;
	default:
		break;
	}

	m_pRigidBody.lock()->EnableCollision(false);
	m_Subject = m_pOwner->GetComponent <Qnity::SubjectComponent>();

}

void Bubble::Update(int MsPerFrame)
{
	ProjectileCollider::Update(MsPerFrame);

}

void Bubble::LateUpdate(int MsPerFrame)
{
	ProjectileCollider::LateUpdate(MsPerFrame);
}

void Bubble::Render() const
{

}

void Bubble::HitEvent(Qnity::GameObject* other, bool terrain)
{
	UNREFERENCED_PARAMETER(terrain);
	UNREFERENCED_PARAMETER(other);

	if (m_TimeElapsed > m_LifeTime && terrain)
	{
		m_pOwner->SetDestroy();

		if (m_Trapped == EnemyTrapped::zenchang)
		{
			std::shared_ptr<Qnity::GameObject> zenchanObject = std::make_shared<Qnity::GameObject>();
			std::shared_ptr<Qnity::Texture2DComponent> pTextureZenChan = std::make_shared<Qnity::Texture2DComponent>(1, true);
			pTextureZenChan->SetDisplaySize(64, 64);
			std::shared_ptr<ZenChanBehavior> pZenChanBehavior = std::make_shared<ZenChanBehavior>();
			std::shared_ptr<ZenChanInput> pZenChanInput = std::make_shared<ZenChanInput>(m_pPlayer1, m_pPlayer2);
			std::shared_ptr<Qnity::RigidBodyComponent> rbZenChan = std::make_shared<Qnity::RigidBodyComponent>(m_pMap);
			rbZenChan->SetColliderGroup(Qnity::ColliderTarget::badGuys);
			std::shared_ptr<EnemyCollider> pEnemyCollider = std::make_shared<EnemyCollider>(m_pMap);

			zenchanObject->SetPosition(m_pOwner->GetTransform().GetPosition());
			zenchanObject->AddComponent(pTextureZenChan);
			zenchanObject->AddComponent(pZenChanBehavior);
			zenchanObject->AddComponent(pZenChanInput);
			zenchanObject->AddComponent(rbZenChan);
			zenchanObject->AddComponent(pEnemyCollider);

			m_pMap->MarkForAdd(zenchanObject);
		}
	}
	else if(!terrain)
	{
		//score points if enemy was trapped, and spawn food
		m_pOwner->SetDestroy();

		if (m_Trapped != EnemyTrapped::none)
		{
			CreateFood(m_Trapped);
			m_Subject.lock()->Notify(Qnity::Event::ENEMY_KILLED, 0);
		}
	}
}

void Bubble::CreateFood(EnemyTrapped type)
{
	std::shared_ptr<Qnity::GameObject> foodObject = std::make_shared<Qnity::GameObject>();
	std::shared_ptr<Qnity::Texture2DComponent> foodTexture = std::make_shared<Qnity::Texture2DComponent>(1, true);
	foodTexture->SetDisplaySize(48, 48);
	std::shared_ptr<FoodCollider> pFoodCollider = std::make_shared<FoodCollider>(m_pMap, type);
	std::shared_ptr<Qnity::RigidBodyComponent> rb = std::make_shared<Qnity::RigidBodyComponent>(m_pMap, 48.f);
	rb->SetColliderGroup(Qnity::ColliderTarget::goodGuys);
	std::shared_ptr<Qnity::SubjectComponent> subject = std::make_shared<Qnity::SubjectComponent>();

	//set correct sprite
	foodTexture->SetTextureNumber(4);
	foodTexture->SetHalfOffset(true);
	switch (type)
	{
	case EnemyTrapped::none:
		break;
	case EnemyTrapped::zenchang:
		foodTexture->SetSpriteNumber(207);
		break;
	case EnemyTrapped::maita:
		foodTexture->SetSpriteNumber(203);
		break;
	default:
		break;
	}

	foodObject->SetPosition(m_pOwner->GetTransform().GetPosition());
	foodObject->AddComponent(foodTexture);
	foodObject->AddComponent(pFoodCollider);
	foodObject->AddComponent(rb);
	foodObject->AddComponent(subject);

	m_pMap->MarkForAdd(foodObject);
}