#include "QnityPCH.h"
#include "PlayerBehavior.h"

PlayerBehavior::PlayerBehavior(int player)
	: ActorBehavior{}
	, m_PlayerNumber{player}
	, m_ShootTimer{}
	, m_ShootCooldown{512}
	, m_Lives{4}
	, m_InvulnerabilityTimer{0}
	, m_MaxInvulnerabilityTime{512}
{

}

PlayerBehavior::~PlayerBehavior()
{

}

void PlayerBehavior::Initialize()
{
	ActorBehavior::Initialize();

	//init sprite
	if (m_PlayerNumber == 0)
	{
		m_pTextureComponent.lock()->SetSpriteNumber(0);
	}
	else
	{
		m_pTextureComponent.lock()->SetSpriteNumber(30);
		m_SpriteOffset = 30;
	}
	m_pTextureComponent.lock()->SetTextureNumber(int(PlayerTextures::walking));

	m_Subject = m_pOwner->GetComponent <Qnity::SubjectComponent> ();
}

void PlayerBehavior::Update(int MsPerFrame)
{
	ActorBehavior::Update(MsPerFrame);

	if (m_ShootTimer > 0)
	{
		m_ShootTimer -= MsPerFrame;
	}

	if (m_InvulnerabilityTimer > 0)
	{
		m_InvulnerabilityTimer -= MsPerFrame;
	}
}

void PlayerBehavior::LateUpdate(int MsPerFrame)
{
	ActorBehavior::LateUpdate(MsPerFrame);
}

void PlayerBehavior::Render() const
{

}

void PlayerBehavior::SetState(Qnity::ActorState state)
{
	Qnity::ActorBehavior::SetState(state);

	if (m_ShowAttackAnimation)
	{
		return;
	}

	switch (m_State)
	{
	case Qnity::ActorState::idle:
	case Qnity::ActorState::falling:
	case Qnity::ActorState::dropDown:
	case Qnity::ActorState::jumping:
		if (m_PlayerNumber == 0)
		{
			m_SpriteOffset = 0;
		}
		else
		{
			m_SpriteOffset = 30;
		}
		m_SpriteDirectionOffset = 15;
		m_pTextureComponent.lock()->SetTextureNumber(int(PlayerTextures::walking));
		break;

	case Qnity::ActorState::attacking:
		if (m_PlayerNumber == 0)
		{
			m_SpriteOffset = 60;
		}
		else
		{
			m_SpriteOffset = 120;
		}
		m_SpriteDirectionOffset = 30;
		m_pTextureComponent.lock()->SetTextureNumber(int(PlayerTextures::shooting));
		m_ShowAttackAnimation = true;
		break;
	default:
		break;
	}
}

bool PlayerBehavior::CanShoot()
{
	if (m_ShootTimer <= 0)
	{
		m_ShootTimer = m_ShootCooldown;
		return true;
	}
	else
	{
		return false;
	}
}

void PlayerBehavior::LoseLife(int amount)
{
	if (m_InvulnerabilityTimer <= 0)
	{
		m_InvulnerabilityTimer = m_MaxInvulnerabilityTime;
		m_Lives -= amount;

		if (m_Lives <= 0)
		{
			m_pOwner->SetDestroy();
			m_Subject.lock()->Notify(Qnity::Event::GAME_OVER, 0);
		}
	}
}
