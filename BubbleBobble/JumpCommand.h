#pragma once
#include "Command.h"
#include "RigidBodyComponent.h"


class JumpCommand : public Qnity::Command
{
public:
	JumpCommand(Qnity::GameObject* owner);
	virtual ~JumpCommand() = default;
	void Execute() override;
	void Initialize() override;

private:
	std::weak_ptr<Qnity::RigidBodyComponent> m_pRb;
	float m_JumpForce;
};
