#include "LevelObserver.h"
#include "LevelScene.h"

LevelObserver::LevelObserver(LevelScene* scene)
	: m_Scene{ scene }
{

}

void LevelObserver::OnNotify(Qnity::Event event, int value)
{
	switch (event)
	{
	case Qnity::Event::SCORED_POINTS:
		m_Scene->AddScore(value);
		break;
	case Qnity::Event::ENEMY_KILLED:
		m_Scene->DecreaseEnemies();
		break;
	case Qnity::Event::GAME_OVER:
		m_Scene->GameOver();
		break;
	default:
		break;
	}
}

