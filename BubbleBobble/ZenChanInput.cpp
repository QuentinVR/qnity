#include "QnityPCH.h"
#include "ZenChanInput.h"
#include "RigidBodyComponent.h"
#include "ActorBehavior.h"
#include "ChargeCommand.h"

ZenChanInput::ZenChanInput(std::weak_ptr<Qnity::GameObject> player1, std::weak_ptr<Qnity::GameObject> player2)
	: AgentInputComponent{player1, player2}
	, m_GoRight{true}
	, m_JumpTimer{}
	, m_JumpCooldown{4500}
	, m_Player2Present{false}
{

}

ZenChanInput::~ZenChanInput()
{

}

void ZenChanInput::Update(int MsPerFrame)
{
	AgentInputComponent::Update(MsPerFrame);


	//if player is on same height, charge at player
	Qnity::Vector2<float> playerPos1 = m_pPlayer1.lock()->GetTransform().GetPosition();


	Qnity::Vector2<float> zenchanPos = m_pActor.lock()->GetPos(true);

	if (abs(playerPos1.y - zenchanPos.y) < 40.f)
	{
		if (playerPos1.x < zenchanPos.x && !m_GoRight)
		{
			m_pNextCommands.push_back(m_pCommandSet[int(ZenChanCommands::Charge)]);
		}
		else if(playerPos1.x > zenchanPos.x && m_GoRight)
		{
			m_pNextCommands.push_back(m_pCommandSet[int(ZenChanCommands::Charge)]);
		}
	}
	else if(m_Player2Present)
	{
		Qnity::Vector2<float> playerPos2 = m_pPlayer2.lock()->GetTransform().GetPosition();
		if (abs(playerPos2.y - zenchanPos.y) < 40.f)
		{
			if (playerPos2.x < zenchanPos.x && !m_GoRight)
			{
				m_pNextCommands.push_back(m_pCommandSet[int(ZenChanCommands::Charge)]);
			}
			else if (playerPos2.x > zenchanPos.x&& m_GoRight)
			{
				m_pNextCommands.push_back(m_pCommandSet[int(ZenChanCommands::Charge)]);
			}
		}
	}
	else
	{
		//count till jump
		m_JumpTimer += MsPerFrame;

		if (m_JumpTimer > m_JumpCooldown)
		{
			m_JumpTimer -= m_JumpCooldown;
			m_pNextCommands.push_back(m_pCommandSet[int(ZenChanCommands::Jump)]);
		}
	}

	if (m_GoRight)
	{
		m_pNextCommands.push_back(m_pCommandSet[int(ZenChanCommands::MoveRight)]);
	}
	else
	{
		m_pNextCommands.push_back(m_pCommandSet[int(ZenChanCommands::MoveLeft)]);
	}

	//move and switch direction when bumping into something
	if (m_pRigidBody.lock()->HasHitTerrainThisFrame())
	{
		m_GoRight = !m_GoRight;
	}
}

void ZenChanInput::LateUpdate(int MsPerFrame)
{
	AgentInputComponent::LateUpdate(MsPerFrame);
}

void ZenChanInput::Render() const
{
	AgentInputComponent::Render();
}

void ZenChanInput::Initialize()
{
	//add commands to list
	m_pCommandSet.push_back(new MoveCommand{m_pOwner, -128.f});
	m_pCommandSet.push_back(new MoveCommand{m_pOwner, 128.f});
	m_pCommandSet.push_back(new JumpCommand{m_pOwner});
	m_pCommandSet.push_back(new ChargeCommand{m_pOwner});

	m_pRigidBody = m_pOwner->GetComponent<Qnity::RigidBodyComponent>();

	if (m_pPlayer2.lock() != nullptr)
	{
		m_Player2Present = true;
	}


	AgentInputComponent::Initialize();
}
