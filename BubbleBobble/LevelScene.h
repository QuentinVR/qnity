#pragma once
#include "Scene.h"
#include "SceneManager.h"
#include <GameObject.h>
#include <Map.h>
#include "TextComponent.h"
#include <thread>

class LevelObserver;

enum class TextureNames
{
	blocks = 0,
	sprites0 = 1,
	sprites1 = 2,
	sprites2 = 3,
	sprites3 = 4,
	sprites4 = 5,
	sprites5 = 6,
};

class LevelScene : public Qnity::Scene
{
public:

	LevelScene(const std::string& name, int levelNumber, int numberOfPlayers);
	virtual ~LevelScene();
	LevelScene(const LevelScene& other) = delete;
	LevelScene(LevelScene&& other) = delete;
	LevelScene& operator=(const LevelScene& other) = delete;
	LevelScene& operator=(LevelScene&& other) = delete;

	void Update(int MsPerFrame) override;
	void Render() const override;
	void LateUpdate(int MsPerFrame) override;

	void Initialize() override;

	void AddPlayers(int number);
	void AddZenChan(Qnity::Vector2<float> offset);
	void LoadMap();

	int GetLevelNumber();

	std::shared_ptr<Qnity::GameObject> GetPlayer(int number);
	void SetPlayer(std::shared_ptr<Qnity::GameObject> player, int number);

	void DecreaseEnemies();
	void AddScore(int score);
	void SetScore(int score);
	int GetScore();
	void GameOver();

	void LoadTextures();

	void Add(const std::shared_ptr<Qnity::GameObject>& object) override;

	void CreateNextLevel();

private:

	std::shared_ptr<Qnity::GameObject> m_pPlayerCharacter1;
	std::shared_ptr<Qnity::GameObject> m_pPlayerCharacter2;

	int m_NumberOfEnemies;
	int m_LevelNumber;
	int m_NumberOfPlayers;

	static bool m_TexturesLoaded;

	int m_Score;

	std::shared_ptr<LevelObserver> m_Observer;
	std::shared_ptr<Qnity::TextComponent> m_TextComponent;

	std::thread m_NextSceneThread;
	std::shared_ptr<LevelScene> m_NextLevel;
};