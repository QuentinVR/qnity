
#if _DEBUG
// ReSharper disable once CppUnusedIncludeDirective
//#include <vld.h>
#endif

#include "Qnity.h"
#include "MyGame.h"
#include <iostream>

int main(int, char*[]) {
	Qnity::MyGame engine;
	engine.Run();
    return 0;
}