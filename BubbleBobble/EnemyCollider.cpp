#include "QnityPCH.h"
#include "EnemyCollider.h"
#include "PlayerBehavior.h"
#include <Logger.h>


EnemyCollider::EnemyCollider(std::shared_ptr<Qnity::Map> map)
	: Qnity::ColliderComponent{ Qnity::ColliderTarget::goodGuys, map }
{

}

EnemyCollider::~EnemyCollider()
{

}

void EnemyCollider::Update(int MsPerFrame)
{
	ColliderComponent::Update(MsPerFrame);
}

void EnemyCollider::LateUpdate(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void EnemyCollider::Render() const
{

}

void EnemyCollider::Initialize()
{
	Qnity::ColliderComponent::Initialize();

}

void EnemyCollider::HitEvent(Qnity::GameObject* other, bool terrain)
{
	UNREFERENCED_PARAMETER(other);
	UNREFERENCED_PARAMETER(terrain);
	
	//damage player
	std::weak_ptr<PlayerBehavior> player = other->GetComponent<PlayerBehavior>();
	if (player.lock() != nullptr)
	{
		player.lock()->LoseLife(1);
	}
	else
	{
		Qnity::Logger().LogWarning("Enemy hit test failed");
	}

}