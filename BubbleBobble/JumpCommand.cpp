#include "JumpCommand.h"
#include <iostream>
#include <windows.h>
#include "ActorBehavior.h"

JumpCommand::JumpCommand(Qnity::GameObject* owner)
	: Command{owner}
	, m_JumpForce{400.f}
{

} 

void JumpCommand::Execute()
{
	if (m_pRb.lock()->GetGrounded())
	{
		m_pRb.lock()->SetYVelocity(-m_JumpForce);
		m_pActor.lock()->SetState(Qnity::ActorState::jumping);
	}
}

void JumpCommand::Initialize()
{
	m_pRb = m_pOwner->GetComponent<Qnity::RigidBodyComponent>();
}