#pragma once
#include "Command.h"
#include <Map.h>
#include "PlayerBehavior.h"

class ShootCommand : public Qnity::Command
{
public:
	ShootCommand(Qnity::GameObject* owner, std::shared_ptr<Qnity::Map> map, int playerNumber, std::weak_ptr<Qnity::GameObject> player1, std::weak_ptr<Qnity::GameObject> player2);
	virtual ~ShootCommand() = default;
	void Execute() override;
	void Initialize() override;

private:
	std::shared_ptr<Qnity::Map> m_pMap;
	std::weak_ptr<PlayerBehavior> m_pPlayerBehavior;
	std::weak_ptr<Qnity::GameObject> m_pPlayer1;
	std::weak_ptr<Qnity::GameObject> m_pPlayer2;
	int m_PlayerNumber;
};
