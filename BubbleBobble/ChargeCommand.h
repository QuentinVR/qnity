#pragma once
#include "Command.h"
#include "RigidBodyComponent.h"


class ChargeCommand : public Qnity::Command
{
public:
	ChargeCommand(Qnity::GameObject* owner);
	virtual ~ChargeCommand() = default;
	void Execute() override;
	void Initialize() override;

private:
	std::weak_ptr<Qnity::RigidBodyComponent> m_pRb;
};
