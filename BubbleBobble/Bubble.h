#pragma once
#include "ProjectileCollider.h"
#include "SubjectComponent.h"

class Qnity::GameObject;

enum class EnemyTrapped
{
	none,
	zenchang,
	maita
};

class Bubble : public Qnity::ProjectileCollider
{
public:
	Bubble(std::shared_ptr<Qnity::Map> map, EnemyTrapped enemy, std::weak_ptr<Qnity::GameObject> player1, std::weak_ptr<Qnity::GameObject> player2);
	virtual ~Bubble();

	Bubble(const Bubble&) = delete;
	Bubble(Bubble&& other) = delete;
	Bubble& operator=(const Bubble& other) = delete;
	Bubble& operator=(Bubble&& other) = delete;

	void Update(int MsPerFrame) override;
	void LateUpdate(int MsPerFrame) override;
	void Render() const override;
	void Initialize() override;

	void HitEvent(Qnity::GameObject* other, bool terrain) override;

private:
	EnemyTrapped m_Trapped;
	std::weak_ptr<Qnity::GameObject> m_pPlayer1;
	std::weak_ptr<Qnity::GameObject> m_pPlayer2;

	void CreateFood(EnemyTrapped type);

	std::weak_ptr<Qnity::SubjectComponent> m_Subject;
};
