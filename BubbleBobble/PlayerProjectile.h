#pragma once
#include "ProjectileCollider.h"

enum class EnemyTrapped;
class Qnity::GameObject;

class PlayerProjectile : public Qnity::ProjectileCollider
{
public:
	PlayerProjectile(Qnity::Vector2<float> velocity, std::shared_ptr<Qnity::Map> map, std::weak_ptr<Qnity::GameObject> player1, std::weak_ptr<Qnity::GameObject> player2);
	virtual ~PlayerProjectile();

	PlayerProjectile(const PlayerProjectile&) = delete;
	PlayerProjectile(PlayerProjectile&& other) = delete;
	PlayerProjectile& operator=(const PlayerProjectile& other) = delete;
	PlayerProjectile& operator=(PlayerProjectile&& other) = delete;

	void HitEvent(Qnity::GameObject* other, bool terrain = false) override;

protected:
	void CreateBubble(EnemyTrapped enemy);
	std::weak_ptr<Qnity::GameObject> m_pPlayer1;
	std::weak_ptr<Qnity::GameObject> m_pPlayer2;
};

