#pragma once
#include "BaseComponent.h"
#include "RigidBodyComponent.h"
#include "Texture2DComponent.h"
#include "ActorBehavior.h"
#include <SubjectComponent.h>

enum class PlayerTextures
{
	walking = 1, shooting = 2, bubble = 4
};

class PlayerBehavior : public Qnity::ActorBehavior
{

public:
	PlayerBehavior(int player);
	virtual ~PlayerBehavior();

	void Initialize() override;
	void Update(int MsPerFrame) override;
	void LateUpdate(int MsPerFrame) override;
	void Render() const override;

	void SetState(Qnity::ActorState state) override;

	bool CanShoot();

	void LoseLife(int amount);

private:
	int m_PlayerNumber;

	int m_ShootTimer;
	int m_ShootCooldown;

	int m_Lives;
	int m_InvulnerabilityTimer;
	int m_MaxInvulnerabilityTime;

	std::weak_ptr<Qnity::SubjectComponent> m_Subject;
};
