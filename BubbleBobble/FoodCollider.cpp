#include "QnityPCH.h"
#include "FoodCollider.h"

FoodCollider::FoodCollider(std::shared_ptr<Qnity::Map> map, EnemyTrapped type)
	: Qnity::ColliderComponent{Qnity::ColliderTarget::goodGuys, map}
	, m_Type{type}
	, m_LifeTime{5000}
	, m_TimeBeforePickup{512}
	, m_HasHitGround{false}
{

}

FoodCollider::~FoodCollider()
{

}

void FoodCollider::Update(int MsPerFrame)
{
	m_LifeTime -= MsPerFrame;

	if (m_LifeTime <= 0)
	{
		m_pOwner->SetDestroy();
	}

	if (m_TimeBeforePickup > 0)
	{
		m_TimeBeforePickup -= MsPerFrame;
	}

	if (!m_HasHitGround)
	{
		m_pRigidBody.lock()->SetXVelocity(m_Velocity.x);
	}

	if (!m_HasHitGround && (m_pRigidBody.lock()->HasHitTerrainThisFrame() || m_pRigidBody.lock()->GetGrounded()))
	{
		m_HasHitGround = true;
		m_pRigidBody.lock()->SetXVelocity(0);
	}
}

void FoodCollider::LateUpdate(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void FoodCollider::Render() const
{

}

void FoodCollider::Initialize()
{
	Qnity::ColliderComponent::Initialize();

	//give some initial random direction
	m_Velocity.x = float(rand() % 64 + 64);
	if (rand() % 1 == 0)
	{
		m_Velocity.x *= -1;
	}
	m_Velocity.y = float(rand() % 64 - 128);

	m_pRigidBody.lock()->SetXVelocity(m_Velocity.x);
	m_pRigidBody.lock()->SetYVelocity(m_Velocity.y);

	m_Subject = m_pOwner->GetComponent <Qnity::SubjectComponent>();

}

void FoodCollider::HitEvent(Qnity::GameObject* other, bool terrain)
{
	UNREFERENCED_PARAMETER(other);
	UNREFERENCED_PARAMETER(terrain);

	if (m_TimeBeforePickup <= 0)
	{
		m_Subject.lock()->Notify(Qnity::Event::SCORED_POINTS, 100);
		m_pOwner->SetDestroy();
	}
}