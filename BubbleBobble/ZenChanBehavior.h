#pragma once
#include "BaseComponent.h"
#include "RigidBodyComponent.h"
#include "Texture2DComponent.h"
#include "ActorBehavior.h"

class ZenChanBehavior : public Qnity::ActorBehavior
{

public:
	ZenChanBehavior();
	virtual ~ZenChanBehavior();

	void Initialize() override;
	void Update(int MsPerFrame) override;
	void LateUpdate(int MsPerFrame) override;
	void Render() const override;

	void SetState(Qnity::ActorState state) override;

private:
	
};
