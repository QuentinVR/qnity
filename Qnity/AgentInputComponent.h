#pragma once
#include "InputComponent.h"

enum class AgentType
{
	ZenChan, Maita
};

namespace Qnity
{
	class AgentInputComponent : public InputComponent
	{
	public:
		AgentInputComponent(std::weak_ptr<GameObject> player1, std::weak_ptr<GameObject> player2);
		virtual ~AgentInputComponent();

		AgentInputComponent(const AgentInputComponent& other) = delete;
		AgentInputComponent(AgentInputComponent&& other) = delete;
		AgentInputComponent& operator=(const AgentInputComponent& other) = delete;
		AgentInputComponent& operator=(AgentInputComponent&& other) = delete;

		void Update(int MsPerFrame) override;
		void LateUpdate(int MsPerFrame) override;
		void Render() const override;
		void Initialize() override;

		void SetAgentType(AgentType type);
		AgentType GetAgentType();

	protected:
		std::vector<Command*> m_pCommandSet;
		std::weak_ptr<GameObject> m_pPlayer1;
		std::weak_ptr<GameObject> m_pPlayer2;

		AgentType m_AgentType;
	};
}