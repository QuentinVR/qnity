#pragma once
#include "Transform.h"
#include "BaseComponent.h"
#include <memory>
#include <vector>
#include "QnityMath.h"

namespace Qnity
{
	class Texture2D;

	class GameObject final
	{
	public:
		void Update(int MsPerFrame);
		void LateUpdate(int MsPerFrame);
		void Render() const;
		void Initialize();

		void AddComponent(std::shared_ptr<BaseComponent> newComponent);

		GameObject();
		virtual ~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

		const Transform& GetTransform() const;

		void SetPosition(float x, float y);
		void SetPosition(const Vector2<float>& vec);
		bool IsCravingDestruction();
		virtual void SetDestroy();
		bool HasActor();

		template<typename T>
		inline std::shared_ptr<T> GetComponent()
		{
			for (size_t i{}; i < m_pComponents.size(); i++)
			{
				auto component = std::dynamic_pointer_cast<T>(m_pComponents[i]);
				if (component)
				{
					return component;
				}
			}

			return nullptr;
		}

	protected:
		Transform m_Transform;
		std::vector<std::shared_ptr<BaseComponent>> m_pComponents;

		bool m_ICraveDestruction;
		bool m_HasActor;

	private:
		bool m_IsInitialized;
	};
}
