#include "QnityPCH.h"
#include "RigidBodyComponent.h"
#include "Renderer.h"
#include "ColliderComponent.h"

const float Qnity::RigidBodyComponent::g_Gravity = 500.f;
const float Qnity::RigidBodyComponent::g_TileSize = 32.f;
const float Qnity::RigidBodyComponent::g_ActorSize = 64.f;


Qnity::RigidBodyComponent::RigidBodyComponent(std::shared_ptr<Map> map, float size)
	: m_pMap{map}
	, m_TestCollision{true}
	, m_Velocity{}
	, m_MaxXSpeed{ 256.f }
	, m_MaxYSpeed{256.f}
	, m_IsGrounded{false}
	, m_HasCollidedThisFrame{false}
	, m_ApplyGravity{true}
	, m_TargetGroup{ColliderTarget::badGuys}
	, m_BoostTime{}
	, m_Boost{}
{
	m_BoundingBox.upperLeft = Vector2<float>{};
	m_BoundingBox.upperRight = Vector2<float>{ size, 0.f };
	m_BoundingBox.lowerLeft = Vector2<float>{ 0.f, size };
	m_BoundingBox.lowerRight = Vector2<float>{ size, size };
}

Qnity::RigidBodyComponent::~RigidBodyComponent()
{

}

void Qnity::RigidBodyComponent::Initialize()
{
	BaseComponent::Initialize();
}

void Qnity::RigidBodyComponent::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
	TestCollisionInScene();
}

void Qnity::RigidBodyComponent::LateUpdate(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);

	m_HasCollidedThisFrame = false;

	//perform movement
	Vector2<float> objectPos{ m_pOwner->GetTransform().GetPosition() };

	//check if off screen
	const float offscreenHeight{ 770 };
	if (objectPos.y > offscreenHeight)
	{
		objectPos.y -= offscreenHeight;
	}

	//get new pos
	Vector2<float> newPos{};

	if (m_BoostTime < 0)
	{
		newPos = objectPos + (m_Velocity * (MsPerFrame / 1000.f));
	}
	else
	{
		newPos = objectPos + (m_Velocity * m_Boost * (MsPerFrame / 1000.f));
		m_BoostTime -= MsPerFrame;
	}

	//create boundingbox of new pos in world space
	Rect<float> worldBox = m_BoundingBox + newPos;
	
	//if currently clipping with terrain, just pass through
	bool initialCollision{false};
	Rect<float> worldBoxCurrent = m_BoundingBox + objectPos;
	if (!m_pMap->IsColliding(worldBoxCurrent, m_Velocity, false, false))
	{
		//otherwise test collision of future pos
		//if collision is enabled, check for walls and platforms
		if (m_TestCollision)
		{
			//test for walls and platforms
			m_HasCollidedThisFrame = m_pMap->IsColliding(worldBox, m_Velocity);
		}
		else
		{
			//only test walls
			m_HasCollidedThisFrame = m_pMap->IsColliding(worldBox, m_Velocity, true);
		}
	}
	else
	{
		initialCollision = true;
	}

	//correct for out of bounds cases
	if (worldBox.upperLeft.x < g_TileSize * 2.f)
	{
		worldBox.upperLeft.x += (g_TileSize * 2.f - worldBox.upperLeft.x);
		m_HasCollidedThisFrame = true;
	}

	if (worldBox.upperLeft.x > 1024.f - g_TileSize * 4.2f)
	{
		worldBox.upperLeft.x -= (worldBox.upperLeft.x - (1024.f - g_TileSize * 4.2f));
		m_HasCollidedThisFrame = true;
	}
	
	//update position
	m_pOwner->SetPosition(worldBox.upperLeft.x, worldBox.upperLeft.y);

	//check if on ground
	worldBox = worldBox + Vector2<float>{0.f, 4.f};
	bool hasCollided = m_pMap->IsColliding(worldBox, m_Velocity, !m_TestCollision, false);
	
	//if on ground, y velocity is 0
	if (hasCollided	&& !initialCollision && m_Velocity.y >= 0.f)
	{
		m_IsGrounded = true;
		m_Velocity.y = 0.f;
	}
	else
	{
		m_IsGrounded = false;
	}

	if (!m_IsGrounded && m_ApplyGravity)
	{
		m_Velocity.y += g_Gravity * (MsPerFrame / 1000.f);
		m_Velocity.y = min(m_Velocity.y, m_MaxYSpeed);
	}

	//reset horizontal velocity
}

void Qnity::RigidBodyComponent::Render() const
{
	////Renderer::GetInstance().DrawRectangle(rect);
	//Renderer::GetInstance().DrawRectangle(m_pMap->GetTile(worldBox.lowerLeft).upperLeftCorner);
	//Renderer::GetInstance().DrawRectangle(m_pMap->GetTile(worldBox.lowerRight).upperLeftCorner);
	//Renderer::GetInstance().DrawRectangle(m_pMap->GetTile(worldBox.upperLeft).upperLeftCorner);
	//Renderer::GetInstance().DrawRectangle(m_pMap->GetTile(worldBox.upperRight).upperLeftCorner);
	//Renderer::GetInstance().DrawRectangle(m_pMap->GetTile(worldBox.upperLeft + Vector2<float>{32.f, 32.f}).upperLeftCorner);

	//Renderer::GetInstance().DrawRectangle(GetBoundingBox(true));
}

void Qnity::RigidBodyComponent::SetMap(std::shared_ptr<Map> map)
{
	m_pMap = map;
}

void Qnity::RigidBodyComponent::SetXVelocity(float x)
{
	m_Velocity.x = x;
}

void Qnity::RigidBodyComponent::SetYVelocity(float y)
{
	m_Velocity.y = y;
}

void Qnity::RigidBodyComponent::SetMaxXSpeed(float speed)
{
	m_MaxXSpeed = speed;
}

void Qnity::RigidBodyComponent::SetMaxYSpeed(float speed)
{
	m_MaxYSpeed = speed;
}

Qnity::Vector2<float> Qnity::RigidBodyComponent::GetVelocity() const
{
	return m_Velocity;
}

bool Qnity::RigidBodyComponent::GetGrounded()
{
	return m_IsGrounded;
}

void Qnity::RigidBodyComponent::EnableCollision(bool stats)
{
	m_TestCollision = stats;
}

bool Qnity::RigidBodyComponent::HasHitTerrainThisFrame()
{
	return m_HasCollidedThisFrame;
}

Qnity::Rect<float> Qnity::RigidBodyComponent::GetBoundingBox(bool inWorld) const
{
	if (inWorld)
	{
		return m_BoundingBox + m_pOwner->GetTransform().GetPosition();
	}
	else
	{
		return m_BoundingBox;
	}
}

void Qnity::RigidBodyComponent::SetGravity(bool stats)
{
	m_ApplyGravity = stats;
}

void Qnity::RigidBodyComponent::TestCollisionInScene()
{
	m_pMap->TestCollidersOverlapping(GetBoundingBox(true), m_TargetGroup, m_pOwner);
}

void Qnity::RigidBodyComponent::SetColliderGroup(ColliderTarget target)
{
	m_TargetGroup = target;
}

void Qnity::RigidBodyComponent::SetBoost(int time, float intensity)
{
	m_Boost = intensity;
	m_BoostTime = time;
}