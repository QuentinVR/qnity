#include "QnityPCH.h"
#include <SDL.h>
#include <SDL_ttf.h>

#include "TextComponent.h"
#include "Renderer.h"
#include "Font.h"
#include "Texture2DComponent.h"
#include "GameObject.h"

Qnity::TextComponent::TextComponent(const std::string& text, const std::shared_ptr<Font>& font) 
	: m_NeedsUpdate(true), m_Text(text), m_Font(font), m_pTexture(nullptr)
{ }

Qnity::TextComponent::~TextComponent()
{
	SDL_DestroyTexture(m_pTexture);
}

void Qnity::TextComponent::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
	if (m_NeedsUpdate)
	{
		const SDL_Color color = { 255,255,255 }; // only white text is supported now
		const auto surf = TTF_RenderText_Blended(m_Font->GetFont(), m_Text.c_str(), color);
		if (surf == nullptr) 
		{
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		}
		m_pTexture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
		if (m_pTexture == nullptr)
		{
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(surf);
		m_NeedsUpdate = false;
	}
}

void Qnity::TextComponent::Render() const
{
	if (m_pTexture != nullptr)
	{
		const auto pos = m_pOwner->GetTransform().GetPosition();
		Renderer::GetInstance().RenderTexture(m_pTexture, pos.x, pos.y);
	}
}

// This implementation uses the "dirty flag" pattern
void Qnity::TextComponent::SetText(const std::string& text)
{
	m_Text = text;
	m_NeedsUpdate = true;
}



