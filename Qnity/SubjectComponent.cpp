#include "QnityPCH.h"
#include "SubjectComponent.h"

Qnity::SubjectComponent::SubjectComponent()
	: m_pObserver{}
{

}

Qnity::SubjectComponent::~SubjectComponent()
{

}

void Qnity::SubjectComponent::AddObserver(std::shared_ptr<Observer> observer)
{
	m_pObserver = observer;
}

void Qnity::SubjectComponent::Notify(Event event, int value)
{
	if (m_pObserver.lock() != nullptr)
	{
		m_pObserver.lock()->OnNotify(event, value);
	}
}