#pragma once
#include "BaseComponent.h"

namespace Qnity
{
	class GameObject;
	class TextComponent;

	class FPSCounter final : public BaseComponent
	{
	public:
		FPSCounter();
		virtual ~FPSCounter();

		FPSCounter(const FPSCounter& other) = delete;
		FPSCounter(FPSCounter&& other) = delete;
		FPSCounter& operator=(const FPSCounter& other) = delete;
		FPSCounter& operator=(FPSCounter&& other) = delete;

		void Update(int MsPerFrame) override;
		void Render() const override;

		void Initialize() override;

	private:
		std::shared_ptr<TextComponent> m_pTextComponent;
		int m_Counter;
		int m_Frames;
	};
}
