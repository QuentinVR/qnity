#include "QnityPCH.h"
#include "FPSCounter.h"
#include "TextComponent.h"
#include <memory>
#include "ResourceManager.h"
#include "GameObject.h"
#include "Logger.h"

Qnity::FPSCounter::FPSCounter()
	: m_Counter{}
	, m_Frames{}
{

}

Qnity::FPSCounter::~FPSCounter()
{

}

void Qnity::FPSCounter::Update(int MsPerFrame)
{
	if (!m_IsInitialized)
	{
		Logger().LogError("BaseComponent was not initialized.");
		return;
	}

	m_Counter += MsPerFrame;
	m_Frames++;
	if (m_Counter > 1000 && m_pTextComponent != nullptr)
	{
		m_pTextComponent->SetText(std::to_string(m_Frames));
		m_Counter -= 1000;
		m_Frames = 0;
	}
}

void Qnity::FPSCounter::Render() const
{
	
}

void Qnity::FPSCounter::Initialize()
{
	//call base init
	BaseComponent::Initialize();

	//set textcomponent
	m_pTextComponent = m_pOwner->GetComponent<TextComponent>();

	if (m_pTextComponent == nullptr)
	{
		Logger().LogError("FPSCounter component could not get Textcomponent from its owner.");
	}
}