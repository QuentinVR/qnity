#pragma once
#include "Transform.h"
#include "BaseComponent.h"
#include <string>

struct SDL_Texture;

namespace Qnity
{
	class Font;

	class TextComponent final : public BaseComponent
	{
	public:
		void Update(int MsPerFrame) override;
		void Render() const override;

		void SetText(const std::string& text);

		explicit TextComponent(const std::string& text, const std::shared_ptr<Font>& font);
		virtual ~TextComponent();
		TextComponent(const TextComponent& other) = delete;
		TextComponent(TextComponent&& other) = delete;
		TextComponent& operator=(const TextComponent& other) = delete;
		TextComponent& operator=(TextComponent&& other) = delete;
	private:
		bool m_NeedsUpdate;
		std::string m_Text;

		std::shared_ptr<Font> m_Font;
		SDL_Texture* m_pTexture;
	};
}
