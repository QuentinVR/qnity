#include "QnityPCH.h"
#include "Transform.h"

void Qnity::Transform::SetPosition(const float x, const float y)
{
	m_Position.x = x;
	m_Position.y = y;
}

const Qnity::Vector2<float>& Qnity::Transform::GetPosition() const
{
	return m_Position;
}

