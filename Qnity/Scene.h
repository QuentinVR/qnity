#pragma once
#include "SceneManager.h"
#include "GameObject.h"
#include <string>

namespace Qnity
{
	class Map;

	class Scene
	{
		friend Scene& SceneManager::AddScene(std::shared_ptr<Scene> newScene);
	public:
		virtual void Add(const std::shared_ptr<GameObject>& object);
		void AddDuringGame(const std::shared_ptr<GameObject>& object);

		virtual void Update(int MsPerFrame) = 0;
		virtual void LateUpdate(int MsPerFrame) = 0;
		virtual void Render() const = 0;

		virtual void Initialize() = 0;

		void RootUpdate(int MsPerFrame);
		void RootLateUpdate(int MsPerFrame);
		void RootInitialize();
		void RootRender() const;

		std::vector<std::shared_ptr<GameObject>>& GetObjectsInScene();

		explicit Scene(const std::string& name);
		~Scene();
		Scene(const Scene& other) = delete;
		Scene(Scene&& other) = delete;
		Scene& operator=(const Scene& other) = delete;
		Scene& operator=(Scene&& other) = delete;

	private: 
		std::string m_Name;
		std::vector < std::shared_ptr<GameObject>> m_Objects{};
		std::vector < std::shared_ptr<GameObject>> m_ObjectsToAdd{};

		static unsigned int m_IdCounter; 
		bool m_IsInitialized{false};

	protected:
		std::shared_ptr<Map> m_pMap;
	};

}
