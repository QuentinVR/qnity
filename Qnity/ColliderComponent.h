#pragma once
#include "BaseComponent.h"
#include "QnityMath.h"
#include "RigidBodyComponent.h"

namespace Qnity
{
	enum class ColliderTarget
	{
		goodGuys, badGuys
	};

	class ColliderComponent : public BaseComponent
	{
	public:
		ColliderComponent(ColliderTarget target, std::shared_ptr<Map> map);
		virtual ~ColliderComponent();

		ColliderComponent(const ColliderComponent&) = delete;
		ColliderComponent(ColliderComponent&& other) = delete;
		ColliderComponent& operator=(const ColliderComponent& other) = delete;
		ColliderComponent& operator=(ColliderComponent&& other) = delete;

		virtual void Update(int MsPerFrame) override;
		virtual void LateUpdate(int MsPerFrame) override;
		virtual void Render() const override;
		virtual void Initialize() override;

		virtual void HitEvent(GameObject* other, bool terrain);

		Rect<float> GetBoundingBox() const;

		ColliderTarget GetTarget() const;

		virtual void SetDestroy() override;

	protected:

		std::weak_ptr<RigidBodyComponent> m_pRigidBody;
		ColliderTarget m_Target;
		std::shared_ptr<Map> m_pMap;
	};
}
