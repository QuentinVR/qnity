#pragma once
#include "GameObject.h"


namespace Qnity
{
	class ActorBehavior;

	class Command
	{
	public:
		Command(GameObject* owner);
		virtual ~Command() = default;
		virtual void Execute() = 0;
		virtual void Initialize() = 0;
		virtual void RootInitialize();

	protected:
		GameObject* m_pOwner;
		std::weak_ptr<ActorBehavior> m_pActor;

		bool m_IsInitialized;
	};
}