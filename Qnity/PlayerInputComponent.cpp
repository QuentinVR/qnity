#include "QnityPCH.h"
#include "PlayerInputComponent.h"
#include "Logger.h"

Qnity::PlayerInputComponent::PlayerInputComponent(int player)
	: m_pActions{}
	, m_PlayerNumber{player}
{
	
}

Qnity::PlayerInputComponent::~PlayerInputComponent()
{
	for (InputAction* action : m_pActions)
	{
		delete action;
	}

	m_pActions.clear();
}

void Qnity::PlayerInputComponent::Initialize()
{
	InputComponent::Initialize();

	for (InputAction* action : m_pActions)
	{
		action->command->RootInitialize();
	}
}

void Qnity::PlayerInputComponent::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);

	auto& inputManager = InputManager::GetInstance();
	for (InputAction* action : m_pActions)
	{
		//if button is pressed, add it to next commands
		if (inputManager.InputState(action->gamepadInput, action->keyboardInput, action->inputType, action->isPadPressed, action->isKeyPressed, m_PlayerNumber))
		{
			m_pNextCommands.push_back(action->command);
		}
	}
}

void Qnity::PlayerInputComponent::LateUpdate(int MsPerFrame)
{
	InputComponent::LateUpdate(MsPerFrame);
}

void Qnity::PlayerInputComponent::Render() const
{

}

void Qnity::PlayerInputComponent::RegisterInput(InputAction* inputAction)
{
	m_pActions.push_back(inputAction);
}
