#pragma once

namespace Qnity
{
	enum class Event
	{
		SCORED_POINTS,
		ENEMY_KILLED,
		GAME_OVER
	};

	class Observer
	{
	public:
		virtual ~Observer() = default;
		virtual void OnNotify(Event event, int value) = 0;

	};
}
