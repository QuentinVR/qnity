#pragma once
#include "BaseComponent.h"
#include "Observer.h"

namespace Qnity
{
	class SubjectComponent : public BaseComponent
	{
	public:
		SubjectComponent();
		virtual ~SubjectComponent();

		SubjectComponent(const SubjectComponent& other) = delete;
		SubjectComponent(SubjectComponent&& other) = delete;
		SubjectComponent& operator=(const SubjectComponent& other) = delete;
		SubjectComponent& operator=(SubjectComponent&& other) = delete;

		void AddObserver(std::shared_ptr<Observer> observer);

		void Notify(Event event, int value);


	protected:
		std::weak_ptr<Observer> m_pObserver;
	};
}