#include "QnityPCH.h"
#include "SceneManager.h"
#include "Scene.h"
#include "Logger.h"

void Qnity::SceneManager::Update(int MsPerFrame)
{
	if (m_Scenes.size() <= m_IndexCurrentScene)
	{
		Logger().LogError("Can not update scene with index " + std::to_string(m_IndexCurrentScene) + ", index not present");
	}
	m_Scenes[m_IndexCurrentScene]->RootUpdate(MsPerFrame);
}

void Qnity::SceneManager::LateUpdate(int MsPerFrame)
{
	if (m_Scenes.size() <= m_IndexCurrentScene)
	{
		Logger().LogError("Can not late update scene with index " + std::to_string(m_IndexCurrentScene) + ", index not present");
	}
	m_Scenes[m_IndexCurrentScene]->RootLateUpdate(MsPerFrame);
}

void Qnity::SceneManager::Render()
{
	if (m_Scenes.size() <= m_IndexCurrentScene)
	{
		Logger().LogError("Can not render scene with index " + std::to_string(m_IndexCurrentScene) + ", index not present");
	}

	m_Scenes[m_IndexCurrentScene]->RootRender();
}

Qnity::Scene& Qnity::SceneManager::AddScene(std::shared_ptr<Scene> newScene)
{
	//check if scene not added already

	auto it = std::find(m_Scenes.begin(), m_Scenes.end(), newScene);

	if (it == m_Scenes.end())
	{
		m_Scenes.push_back(newScene);
	}

	newScene->RootInitialize();

	return *newScene;
}

Qnity::Scene& Qnity::SceneManager::GetCurrentScene()
{
	if (m_Scenes.size() <= m_IndexCurrentScene)
	{
		Logger().LogError("Can not return scene with index " + std::to_string(m_IndexCurrentScene) + ", index not present");
	}

	return *m_Scenes[m_IndexCurrentScene];
}

void Qnity::SceneManager::GoToNextLevel()
{
	m_IndexCurrentScene++;
}


