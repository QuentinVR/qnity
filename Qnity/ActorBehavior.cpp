#include "QnityPCH.h"
#include "ActorBehavior.h"

Qnity::ActorBehavior::ActorBehavior()
	: m_State{ ActorState::idle }
	, m_StateChange{false}
	, m_SpriteOffset{}
	, m_SpriteIncrement{}
	, m_TotalAnimationSprites{8}
	, m_AnimationFrameTime{128}
	, m_AnimationTimeElapsed{}
	, m_SpriteDirectionOffset{15}
	, m_LookingRight{true}
	, m_AttackAnimationTime{384}
	, m_AttackAnimationTimeElapsed{0}
{

}

Qnity::ActorBehavior::~ActorBehavior()
{

}

void Qnity::ActorBehavior::Initialize()
{
	//init components
	BaseComponent::Initialize();

	m_pRigidBody = m_pOwner->GetComponent<Qnity::RigidBodyComponent>();
	m_pTextureComponent = m_pOwner->GetComponent<Qnity::Texture2DComponent>();
	m_pInputComponent = m_pOwner->GetComponent<Qnity::InputComponent>();
}

void Qnity::ActorBehavior::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);

	//prevent moving when idle
	m_pRigidBody.lock()->SetXVelocity(0.f);

	//execute behavior
	for (Qnity::Command* command : m_pInputComponent.lock()->GetCurrentCommands())
	{
		command->Execute();
	}

	float dir{ m_pRigidBody.lock()->GetVelocity().x };
	if (dir < -0.1f)
	{
		m_LookingRight = false;
	}
	else if (dir > 0.1f)
	{
		m_LookingRight = true;
	}

	if (m_AttackAnimationTimeElapsed > 0)
	{
		m_AttackAnimationTimeElapsed -= MsPerFrame;
		if (m_AttackAnimationTimeElapsed <= 0)
		{
			m_ShowAttackAnimation = false;
			SetState(ActorState::idle);
		}
	}

	AnimationCycle(MsPerFrame);
}

void Qnity::ActorBehavior::LateUpdate(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);

	//dont check this if shooting
	//set falling state if not holding down anymore
	//or if jumping and velocity goes down
	//or if falling from ledge
	float yVel = m_pRigidBody.lock()->GetVelocity().y;

	if (!m_StateChange && m_State == ActorState::dropDown 
		|| m_State == ActorState::jumping && yVel >= 0.f
		|| !m_pRigidBody.lock()->GetGrounded() && yVel > 0.f && m_State != ActorState::jumping && m_State != ActorState::dropDown
		)
	{
		SetState(ActorState::falling);
	}
	else if(m_pRigidBody.lock()->GetGrounded())
	{
		SetState(ActorState::idle);
	}

	m_StateChange = false;
}

void Qnity::ActorBehavior::Render() const
{

}

void Qnity::ActorBehavior::SetState(ActorState state)
{
	m_State = state;

	switch (state)
	{
	case Qnity::ActorState::idle:
		m_pRigidBody.lock()->EnableCollision(true);
		break;
	case Qnity::ActorState::jumping:
		m_pRigidBody.lock()->EnableCollision(false);
		break;
	case Qnity::ActorState::attacking:
		m_AttackAnimationTimeElapsed = m_AttackAnimationTime;
		break;
	case Qnity::ActorState::falling:
		m_pRigidBody.lock()->EnableCollision(true);
		break;
	case Qnity::ActorState::dropDown:
		m_pRigidBody.lock()->EnableCollision(false);
		break;
	default:
		break;
	}

	m_StateChange = true;
}

void Qnity::ActorBehavior::AnimationCycle(int MsPerFrame)
{
	if (abs(m_pRigidBody.lock()->GetVelocity().x) > 0.f)
	{
		m_AnimationTimeElapsed += MsPerFrame;
		if (m_AnimationFrameTime < m_AnimationTimeElapsed)
		{
			m_AnimationTimeElapsed -= m_AnimationFrameTime;
			m_SpriteIncrement++;
			m_SpriteIncrement %= m_TotalAnimationSprites;
		}
	}
	
	if (!m_LookingRight)
	{
		m_pTextureComponent.lock()->SetSpriteNumber(m_SpriteOffset + m_SpriteIncrement * 2 + m_SpriteDirectionOffset);
	}
	else
	{
		m_pTextureComponent.lock()->SetSpriteNumber(m_SpriteOffset + m_SpriteIncrement * 2);
	}

}

bool Qnity::ActorBehavior::IsLookingRight()
{
	return m_LookingRight;
}

Qnity::Vector2<float> Qnity::ActorBehavior::GetPos(bool center)
{
	if (center)
	{
		return 	 m_pOwner->GetTransform().GetPosition() + m_pRigidBody.lock()->GetBoundingBox(false).lowerRight / 2.f;
	}

	return 	m_pOwner->GetTransform().GetPosition();
}