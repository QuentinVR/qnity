#include "QnityPCH.h"
#include "Texture2DComponent.h"
#include "ResourceManager.h"
#include <SDL.h>
#include "GameObject.h"
#include "Renderer.h"
#include <SDL_render.h>
#include "TextureBank.h"

Qnity::Texture2DComponent::Texture2DComponent(size_t textureNumber, bool isSprite)
	: m_IsSprite{ isSprite }
	, m_SpriteNumber{}
	, m_Columns{15}
	, m_Rows{16}
	, m_OffsetX{}
	, m_OffsetY{}
	, m_DisplayWidth{32}
	, m_DisplayHeight{32}
	, m_TextureNumber{ textureNumber }
	, m_pTexture{}
	, m_HalfOffset{false}
{	
	m_pTexture = TextureBank::GetInstance().GetSDLTexture(int(textureNumber));
	SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_TextureWidth, &m_TextureHeight);
}

Qnity::Texture2DComponent::~Texture2DComponent()
{

}

void Qnity::Texture2DComponent::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void Qnity::Texture2DComponent::Render() const
{
	const auto pos = m_pOwner->GetTransform().GetPosition();

	if (m_IsSprite)
	{
		SDL_Rect source;
		source.w = m_TextureWidth / int(m_Columns);
		source.h = m_TextureHeight / int(m_Rows);
		source.x = (int(m_SpriteNumber) % int(m_Columns)) * source.w;
		source.y = (int(m_SpriteNumber) / int(m_Columns)) * source.h;

		if (m_HalfOffset)
		{
			source.x -= 5;
		}

		SDL_Rect dest;
		dest.x = static_cast<int>(pos.x + m_OffsetX);
		dest.y = static_cast<int>(pos.y + m_OffsetY);
		dest.w = m_DisplayWidth;
		dest.h = m_DisplayHeight;

		Renderer::GetInstance().RenderTexture(m_pTexture, source, dest);
	}
	else
	{
		Renderer::GetInstance().RenderTexture(m_pTexture, pos.x + m_OffsetX, pos.y + m_OffsetY);
	}
}

void Qnity::Texture2DComponent::RenderAtPos(int x, int y) const
{
	if (m_IsSprite)
	{
		SDL_Rect source;
		source.w = m_TextureWidth / int(m_Columns);
		source.h = m_TextureHeight / int(m_Rows);
		source.x = (int(m_SpriteNumber) % int(m_Columns)) * source.w;
		source.y = (int(m_SpriteNumber) / int(m_Columns)) * source.h;

		if (m_HalfOffset)
		{
			source.x -= 5;
		}

		SDL_Rect dest;
		dest.x = static_cast<int>(x);
		dest.y = static_cast<int>(y);
		dest.w = m_DisplayWidth;
		dest.h = m_DisplayHeight;

		Renderer::GetInstance().RenderTexture(m_pTexture, source, dest);
	}
	else
	{
		Renderer::GetInstance().RenderTexture(m_pTexture, float(x), float(y));
	}
}

void Qnity::Texture2DComponent::SetSpriteNumber(size_t number)
{
	m_SpriteNumber = number;
}

void Qnity::Texture2DComponent::SetTextureNumber(size_t number)
{
	m_TextureNumber = number;
	m_pTexture = TextureBank::GetInstance().GetSDLTexture(int(number));
	SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_TextureWidth, &m_TextureHeight);
}

void Qnity::Texture2DComponent::SetColumns(size_t number)
{
	m_Columns = number;
}

void Qnity::Texture2DComponent::SetRows(size_t number)
{
	m_Rows = number;
}


void Qnity::Texture2DComponent::SetOffset(float x, float y)
{
	m_OffsetX = x;
	m_OffsetY = y;
}

void Qnity::Texture2DComponent::SetDisplaySize(int x, int y)
{
	m_DisplayWidth = x;
	m_DisplayHeight = y;
}

void Qnity::Texture2DComponent::SetHalfOffset(bool stats)
{
	m_HalfOffset = stats;
}