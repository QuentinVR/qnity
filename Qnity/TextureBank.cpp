#include "QnityPCH.h"
#include "TextureBank.h"
#include <SDL.h>
#include "ResourceManager.h"
#include "Logger.h"

Qnity::TextureBank::~TextureBank()
{
	for (SDL_Texture* texture : m_pTextures)
	{
		SDL_DestroyTexture(texture);
	}
}

void Qnity::TextureBank::SetResourcePath(const std::string& filepath)
{
	m_Resourcepath = filepath;
}

SDL_Texture* Qnity::TextureBank::GetSDLTexture(int magicNumber)
{
	if (magicNumber < int(m_pTextures.size()))
	{
		return m_pTextures[magicNumber];
	}
	else
	{
		Logger().LogWarning("Unexisting texture requested");
		return nullptr;
	}
}

void Qnity::TextureBank::AddSDLTexture(const std::string& filename)
{
	m_pTextures.push_back(ResourceManager::GetInstance().LoadTexture(m_Resourcepath + filename));
}