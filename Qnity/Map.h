#pragma once
#include <string>
#include "GameObject.h"
#include "Texture2DComponent.h"
#include "QnityMath.h"

namespace Qnity
{
	class Scene;
	class ColliderComponent;
	enum class ColliderTarget;

	enum class TileType
	{
		EMPTY,
		WALL,
		PLATFORM,
		ILLEGAL
	};

	struct Tile
	{
		TileType type{};
		Vector2<int> upperLeftCorner;

		bool operator==(const Tile& tile)
		{
			return (this->type == tile.type && this->upperLeftCorner == tile.upperLeftCorner);
		}
	};


	class Map final
	{
	public:
		Map(Scene* currentScene);
		~Map();
		
		void LoadTiles(const std::string file, int level);
		void LoadTexture(size_t textureNumber, size_t spriteNumber);
		void Render();

		Tile GetTile(Vector2<float> vec) const;
		Tile GetTile(float x, float y) const;
		bool IsColliding(Rect<float>& currentPos, Vector2<float> travelDir, bool onlyTestWalls = false, bool getLegalPos = true);

		void TrimBoundingBox(Rect<float>& rect);

		void MarkForAdd(std::shared_ptr<GameObject> object);
		void AddCollider(ColliderComponent* collider);
		void RemoveColliders();
		void MarkForRemoval(ColliderComponent* rb);


		bool TestCollidersOverlapping(Rect<float> rect, ColliderTarget target, GameObject* other);

	private:
		static const int m_TileColumns{32};
		static const int m_TileRows{25};

		static const int m_TileSize{32};

		Tile m_Tiles[m_TileColumns * m_TileRows];

		std::shared_ptr<Texture2DComponent> m_pTextureComponent;
	
		Scene* m_pScene;
		std::vector<ColliderComponent*> m_pColliders;
		std::vector<ColliderComponent*> m_pCollidersToRemove;

		bool m_GameIsRunning;
	};
}