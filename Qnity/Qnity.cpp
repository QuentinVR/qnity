#include "QnityPCH.h"
#include "Qnity.h"
#include <chrono>
#include <thread>
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include <SDL.h>
#include "Logger.h"
#include "Scene.h"
#include "GameObject.h"


using namespace std;
using namespace std::chrono;

void Qnity::Qnity::Initialize()
{
	//init logger
	Logger().Initialize();
	Logger().LogINFO("Initializing.");

	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	m_Window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		1024,
		800,
		SDL_WINDOW_OPENGL
	);
	if (m_Window == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	Renderer::GetInstance().Init(m_Window);
}

/**
 * Code constructing the scene world starts here
 */
void Qnity::Qnity::LoadGame() const
{
	SceneManager::GetInstance().GetCurrentScene().Initialize();
}

void Qnity::Qnity::Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(m_Window);
	m_Window = nullptr;
	SDL_Quit();
}

void Qnity::Qnity::Run()
{
	Initialize();

	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("");

	Logger().LogINFO("Loading game.");
	LoadGame();

	{
		auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();
		auto& input = InputManager::GetInstance();
		

		bool doContinue = true;
		auto lastTime = std::chrono::high_resolution_clock::now();
		float lag = 0.0f;

		Logger().LogINFO("Running game loop.");
		while (doContinue)
		{
			const auto currentTime = high_resolution_clock::now();
			float deltaTime = float(std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - lastTime).count());
			lastTime = currentTime;
			lag += deltaTime;

			//check if application closed and mouse input
			doContinue = input.ProcessInput();

			//maintain constant framerate and avoids working with floats (floating point errors)
			while (lag >= MsPerFrame)
			{
				sceneManager.Update(MsPerFrame);

				//use lateupdate to swap buffers
				sceneManager.LateUpdate(MsPerFrame);
				lag -= MsPerFrame;
			}

			renderer.Render();
		}
	}

	Logger().ReportErrors();
	Cleanup();
}
