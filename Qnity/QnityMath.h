#pragma once
//somewhat based on graphics programming vector
#include <iostream>

namespace Qnity
{
	template <class T>
	struct Vector2
	{
		T x;
		T y;

		//constructors
		Vector2<T>() : x{}, y{} {}
		Vector2<T>(T x, T y) : x{ x }, y{ y }{}
		Vector2<T>(const Vector2<T>& vec) : x{ vec.x }, y{ vec.y }{}
		Vector2<T>(Vector2<T>&& vec) noexcept
			: x{ std::move(vec.x) }, y{ std::move(vec.y) } {}


		//some math
		template<typename U>
		inline Vector2<T> operator+(const Vector2<U>& vec) const
		{
			return Vector2<T>(x + static_cast<T>(vec.x), y + static_cast<T>(vec.y));
		}

		template<typename U>
		inline Vector2<T> operator-(const Vector2<U>& vec) const
		{
			return Vector2<T>(x - static_cast<T>(vec.x), y - static_cast<T>(vec.y));
		}

		inline Vector2<T> operator*(T scale) const
		{
			return Vector2<T>(x * scale, y * scale);
		}

		inline Vector2<T> operator/(T scale) const
		{
			const T div = static_cast<T>(1.0f / scale);
			return Vector2<T>(x * div, y * div);
		}

		//compound
		inline Vector2<T>& operator=(const Vector2<T>& vec)
		{
			x = vec.x; y = vec.y;
			return *this;
		}

		inline Vector2<T>& operator+=(const Vector2<T>& vec)
		{
			x += vec.x; y += vec.y;
			return *this;
		}

		inline Vector2<T>& operator-=(const Vector2<T>& vec)
		{
			x -= vec.x; y -= vec.y;
			return *this;
		}

		inline Vector2<T>& operator*=(T scale)
		{
			x *= scale; y *= scale;
			return *this;
		}

		inline Vector2<T>& operator/=(T scale)
		{
			const T div = static_cast<T>(1.0f / scale);
			x *= div; y *= div;
			return *this;
		}

		const float smallValue{ 0.000005f };
		//relational
		inline bool operator==(const Vector2<T>& vec) const
		{
			return float(x - vec.x) < smallValue && float(y - vec.y) < smallValue;
		}

		inline bool operator!=(const Vector2<T>& vec) const
		{
			return !(*this == vec);
		}

		//get distance
		inline T SquaredDistance(const Vector2<T> vec) const
		{
			return x * vec.x + y * vec.y;
		}

		inline T Distance(const Vector2<T> vec) const
		{
			return sqrt(x * vec.x + y * vec.y);
		}

		inline Vector2<T>& Normalize()
		{
			float magnitude = this->Distance(*this);
			*this /= magnitude;
			return *this;
		}

		template<typename U>
		operator Vector2<U>() const
		{
			return Vector2<U>(
				static_cast<U>(this->x),
				static_cast<U>(this->y)
				);
		}

		friend std::ostream& operator<<(std::ostream& os, const Vector2& vec)
		{
			os << "x: " << vec.x << ", y: " << vec.y;
			return os;
		}
	};


	template <class T>
	struct Rect
	{
		Vector2<T> upperLeft;
		Vector2<T> upperRight;
		Vector2<T> lowerLeft;
		Vector2<T> lowerRight;

		bool IsPointInRect(const Vector2<T>& vec)
		{
			return (upperLeft.x < vec.x && upperRight > vec.x && upperLeft.y < vec.x && lowerLeft > vec.x);
		}

		bool IsOverlapping(const Rect<T> rect)
		{
			if (upperRight.x < rect.upperLeft.x || rect.upperRight.x < upperLeft.x)
			{
				return false;
			}

			if (lowerLeft.y < rect.upperLeft.y || rect.lowerLeft.y < upperLeft.y)
			{
				return false;
			}

			return true;
		}

		inline Rect<T>& operator=(const Vector2<T>& rect)
		{
			upperLeft = rect.upperLeft;
			upperRight = rect.upperRight;
			lowerLeft = rect.lowerLeft;
			lowerRight = rect.lowerRight;
			return *this;
		}

		template<typename U>
		inline Rect<T> operator+(const Vector2<U>& vec) const
		{
			Rect<T> rect = *this;
			rect.upperLeft += vec;
			rect.upperRight += vec;
			rect.lowerLeft += vec;
			rect.lowerRight += vec;
			return rect;
		}

		template<typename U>
		inline Rect<T> operator-(const Vector2<U>& vec) const
		{
			Rect<T> rect = *this;
			rect.upperLeft -= vec;
			rect.upperRight -= vec;
			rect.lowerLeft -= vec;
			rect.lowerRight -= vec;
			return rect;
		}
	};

}


