#include "QnityPCH.h"
#include "Map.h"
#include "Reader.h"
#include "Logger.h"
#include "ColliderComponent.h"
#include "QnityMath.h"
#include "Scene.h"
#include "ActorBehavior.h"
#include "Renderer.h"

Qnity::Map::Map(Scene* currentScene)
	: m_Tiles{}
	, m_pScene{currentScene}
	, m_GameIsRunning{false}
{

}

Qnity::Map::~Map()
{

}

void Qnity::Map::LoadTiles(const std::string file, int level)
{
	UNREFERENCED_PARAMETER(level);

	Reader reader(file);

	const int size{ int((m_TileColumns * m_TileRows) / 8) };

	//create char array for level
	char levelData[size];

	//skip size for different level
	reader.MoveReadLocation(level * size);

	reader.Read(levelData);

	int index{};
	for (int i{}; i < size; i++)
	{
		size_t bitPos = 0x80;
		for (int j{}; j < 8; j++)
		{	
			if (levelData[i] & bitPos || (index % m_TileColumns)==30 || (index % m_TileColumns) == 31)
			{
				//if on sides its a wall
				if ((index % m_TileColumns) == 30 || (index % m_TileColumns) == 31
					|| (index % m_TileColumns) == 0 || (index % m_TileColumns) == 1)
				{
					m_Tiles[index].type = TileType::WALL;
				}
				else if (index < 32 || index > 766)
				{
					m_Tiles[index].type = TileType::WALL;
				}
				else
				{
					m_Tiles[index].type = TileType::PLATFORM;
				}

			}
			else
			{
				m_Tiles[index].type = TileType::EMPTY;
			}


			m_Tiles[index].upperLeftCorner.x = (index % m_TileColumns) * m_TileSize;
			m_Tiles[index].upperLeftCorner.y = (index / m_TileColumns) * m_TileSize;

			index++;
			bitPos = bitPos >> 1;
		}
	}
}

void Qnity::Map::LoadTexture(size_t textureNumber, size_t spriteNumber)
{
	m_pTextureComponent = std::make_shared<Texture2DComponent>(textureNumber, true);
	m_pTextureComponent->SetColumns(10);
	m_pTextureComponent->SetRows(10);
	m_pTextureComponent->SetDisplaySize(m_TileSize, m_TileSize);
	m_pTextureComponent->SetSpriteNumber(spriteNumber);
}

void Qnity::Map::Render()
{
	const int size{m_TileColumns * m_TileRows};
	for (int i{}; i < size; i++)
	{
		if (m_Tiles[i].type != TileType::EMPTY)
		{
			m_pTextureComponent->RenderAtPos((i % m_TileColumns) * m_TileSize, (i / m_TileColumns) * m_TileSize);
		}
	}
}

Qnity::Tile Qnity::Map::GetTile(Vector2<float> vec) const
{
	return GetTile(vec.x, vec.y);
}

Qnity::Tile Qnity::Map::GetTile(float x, float y) const
{
	int tileX = int(x / m_TileSize);
	int tileY = int(y / m_TileSize);

	int index{ tileX + tileY * m_TileColumns };

	const int size{ m_TileColumns * m_TileRows };
	if (index > 0 && index < size)
	{
		return m_Tiles[index];
	}
	else
	{
		Logger().LogWarning("illegal tile retrieval");
		return Tile{ TileType::ILLEGAL, Vector2<float>{x, y} };
	}
}

bool Qnity::Map::IsColliding(Rect<float> & currentPos, Vector2<float> travelDir, bool onlyTestWalls, bool getLegalPos)
{
	//check if any of the boundingbox are within a solid tile, if so, collision has happened
	int watchdog{};
	const int tries{5};
	bool hasCollided{false};

	Rect<float> originalPos = currentPos;
	Vector2<float> yComponent{ 0.f, travelDir.y };
	Vector2<float> xComponent{ travelDir.x, 0.f };

	//get sign of component
	
	xComponent.x = (abs(xComponent.x) / xComponent.x);
	yComponent.y = (abs(yComponent.y) / yComponent.y);

	int absX = int(abs(xComponent.x));
	int absY = int(abs(yComponent.y));

	while (watchdog < tries && absY == 1)
	{
		Tile upperLeftTile{ GetTile(currentPos.upperLeft) };
		Tile upperRightTile{ GetTile(currentPos.upperRight) };
		Tile lowerLeftTile{ GetTile(currentPos.lowerLeft) };
		Tile lowerRightTile{ GetTile(currentPos.lowerRight) };
		Tile middleTile{ GetTile(currentPos.upperLeft + Vector2<float>{32.f, 32.f}) };
		Tile middleLeftTile{ GetTile(currentPos.upperLeft + Vector2<float>{0.f, 32.f}) };
		Tile middleRightTile{ GetTile(currentPos.upperLeft + Vector2<float>{64.f, 32.f}) };

		if (upperLeftTile.type == TileType::WALL
			|| upperRightTile.type == TileType::WALL
			|| lowerLeftTile.type == TileType::WALL
			|| lowerRightTile.type == TileType::WALL
			|| middleTile.type == TileType::WALL
			|| middleLeftTile.type == TileType::WALL
			|| middleRightTile.type == TileType::WALL
			|| (upperLeftTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (upperRightTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (lowerLeftTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (lowerRightTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (middleTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (middleLeftTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (middleRightTile.type == TileType::PLATFORM && !onlyTestWalls)
			)
		{
			//has collided
			hasCollided = true;

			if (!getLegalPos)
			{
				return hasCollided;
			}

			currentPos = currentPos - yComponent;

			watchdog++;
		}
		else
		{
			// no collision
			return hasCollided;
		}
	}

	//then try y
	watchdog = 0;
	currentPos = originalPos;
	while (watchdog < tries && absX == 1)
	{
		Tile upperLeftTile{ GetTile(currentPos.upperLeft) };
		Tile upperRightTile{ GetTile(currentPos.upperRight) };
		Tile lowerLeftTile{ GetTile(currentPos.lowerLeft) };
		Tile lowerRightTile{ GetTile(currentPos.lowerRight) };
		Tile middleTile{ GetTile(currentPos.upperLeft + Vector2<float>{32.f, 32.f}) };
		Tile middleLeftTile{ GetTile(currentPos.upperLeft + Vector2<float>{0.f, 32.f}) };
		Tile middleRightTile{ GetTile(currentPos.upperLeft + Vector2<float>{64.f, 32.f}) };

		if (upperLeftTile.type == TileType::WALL
			|| upperRightTile.type == TileType::WALL
			|| lowerLeftTile.type == TileType::WALL
			|| lowerRightTile.type == TileType::WALL
			|| middleTile.type == TileType::WALL
			|| middleLeftTile.type == TileType::WALL
			|| middleRightTile.type == TileType::WALL
			|| (upperLeftTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (upperRightTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (lowerLeftTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (lowerRightTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (middleTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (middleLeftTile.type == TileType::PLATFORM && !onlyTestWalls)
			|| (middleRightTile.type == TileType::PLATFORM && !onlyTestWalls)
			)
		{
			//has collided
			hasCollided = true;

			if (!getLegalPos)
			{
				return hasCollided;
			}

			currentPos = currentPos - xComponent;

			watchdog++;
		}
		else
		{
			// no collision
			return hasCollided;
		}
	}

	//Logger().LogWarning("Collisiondetection ran out of tries");
	currentPos = originalPos;

	return true;
}

void Qnity::Map::TrimBoundingBox(Rect<float>& rect)
{
	float const trim{ 32.f };
	rect.upperRight.x -= trim;
	rect.lowerRight.x -= trim;
	rect.lowerRight.y -= trim;
	rect.lowerLeft.y -= trim;
}

void Qnity::Map::MarkForAdd(std::shared_ptr<GameObject> object)
{
	m_pScene->AddDuringGame(object);
}

void Qnity::Map::AddCollider(ColliderComponent* collider)
{
	auto it = std::find(m_pColliders.begin(), m_pColliders.end(), collider);
	if (it == m_pColliders.end())
	{
		m_pColliders.push_back(collider);
	}
}

void Qnity::Map::RemoveColliders()
{
	//if not already included, include it
	for (ColliderComponent* collider : m_pCollidersToRemove)
	{
		auto it = std::find(m_pColliders.begin(), m_pColliders.end(), collider);
		if (it != m_pColliders.end())
		{
			m_pColliders.at(it - m_pColliders.begin()) = *(m_pColliders.end() - 1);
			m_pColliders.pop_back();
		}
	}

	m_pCollidersToRemove.clear();
}

void Qnity::Map::MarkForRemoval(ColliderComponent* collider)
{
	//make sure ist not already marked for removal

	auto it = std::find(m_pCollidersToRemove.begin(), m_pCollidersToRemove.end(), collider);
	if (it == m_pCollidersToRemove.end())
	{
		m_pCollidersToRemove.push_back(collider);
	}
}

bool Qnity::Map::TestCollidersOverlapping(Rect<float> rect, ColliderTarget target, GameObject* other)
{
	if (!m_GameIsRunning)
	{
		m_GameIsRunning = true;
	}

	for (ColliderComponent* collider : m_pColliders)
	{
		if (collider->GetTarget() == target)
		{
			Rect<float> colliderBox = collider->GetBoundingBox();

			if (colliderBox.IsOverlapping(rect))
			{
				//only allow collision with actors
				if (other->HasActor())
				{
					collider->HitEvent(other, false);
					return true;
				}
			}
		}
	}

	return false;
}
