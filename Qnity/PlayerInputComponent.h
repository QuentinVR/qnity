#pragma once
#include "InputComponent.h"

namespace Qnity
{
	class PlayerInputComponent final : public InputComponent
	{
	public:
		PlayerInputComponent(int player);
		virtual ~PlayerInputComponent();

		PlayerInputComponent(const PlayerInputComponent& other) = delete;
		PlayerInputComponent(PlayerInputComponent&& other) = delete;
		PlayerInputComponent& operator=(const PlayerInputComponent& other) = delete;
		PlayerInputComponent& operator=(PlayerInputComponent&& other) = delete;

		void Update(int MsPerFrame) override;
		void LateUpdate(int MsPerFrame) override;
		void Render() const override;
		void Initialize() override;

		void RegisterInput(InputAction* input);

	private:
		std::vector<InputAction*> m_pActions;

		int m_PlayerNumber;
	};
}