#pragma once
#include "QnityMath.h"

namespace Qnity
{
	class Transform final
	{
	public:
		const Vector2<float>& GetPosition() const;
		void SetPosition(float x, float y);
	private:
		Vector2<float> m_Position;
	};
}
