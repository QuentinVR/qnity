#pragma once
#include "BaseComponent.h"
#include "Map.h"
#include "QnityMath.h"

namespace Qnity
{
	class Scene;
	class ColliderComponent;
	enum class ColliderTarget;

	class RigidBodyComponent final : public BaseComponent
	{
	public:
		RigidBodyComponent(std::shared_ptr<Map> pMap, float size = g_ActorSize);
		virtual ~RigidBodyComponent();

		void Initialize() override;
		void Update(int MsPerFrame) override;
		void LateUpdate(int MsPerFrame) override;
		void Render() const override;

		RigidBodyComponent(const RigidBodyComponent& other) = delete;
		RigidBodyComponent(RigidBodyComponent&& other) = delete;
		RigidBodyComponent& operator=(const RigidBodyComponent& other) = delete;
		RigidBodyComponent& operator=(RigidBodyComponent&& other) = delete;

		void SetMap(std::shared_ptr<Map> map);
		void SetXVelocity(float x);
		void SetYVelocity(float y);
		void SetMaxXSpeed(float speed);
		void SetMaxYSpeed(float speed);
		void EnableCollision(bool stats);

		bool GetGrounded();

		Vector2<float> GetVelocity() const;

		bool HasHitTerrainThisFrame();

		Rect<float> GetBoundingBox(bool inWorld = true) const;

		void SetGravity(bool stats);

		void TestCollisionInScene();

		void SetColliderGroup(ColliderTarget target);

		void SetBoost(int time, float intensity);

	private:

		Vector2<float> m_Velocity;
		float m_MaxXSpeed;
		float m_MaxYSpeed;

		std::shared_ptr<Map> m_pMap;

		static const float g_Gravity;
		static const float g_TileSize;
		static const float g_ActorSize;

		bool m_TestCollision;

		bool m_IsGrounded;
		bool m_HasCollidedThisFrame;
		bool m_ApplyGravity;

		Rect<float> m_BoundingBox;

		ColliderTarget m_TargetGroup;

		int m_BoostTime;
		float m_Boost;
	};
}

