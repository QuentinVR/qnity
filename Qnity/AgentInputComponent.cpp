#include "QnityPCH.h"
#include "AgentInputComponent.h"


Qnity::AgentInputComponent::AgentInputComponent(std::weak_ptr<GameObject> player1, std::weak_ptr<GameObject> player2)
	: InputComponent{}
	, m_pCommandSet{}
	, m_pPlayer1{ player1 }
	, m_pPlayer2{ player2 }
	, m_AgentType{AgentType::ZenChan}
{

}

Qnity::AgentInputComponent::~AgentInputComponent()
{
	for (Command* command : m_pCommandSet)
	{
		delete command;
	}

	m_pCommandSet.clear();
}

void Qnity::AgentInputComponent::Update(int MsPerFrame)
{
	InputComponent::Update(MsPerFrame);
}

void Qnity::AgentInputComponent::LateUpdate(int MsPerFrame)
{
	InputComponent::LateUpdate(MsPerFrame);
}

void Qnity::AgentInputComponent::Render() const
{
	InputComponent::Render();
}

void Qnity::AgentInputComponent::Initialize()
{
	InputComponent::Initialize();

	for (Command* command : m_pCommandSet)
	{
		command->RootInitialize();
	}
}

void Qnity::AgentInputComponent::SetAgentType(AgentType type)
{
	m_AgentType = type;
}

AgentType Qnity::AgentInputComponent::GetAgentType()
{
	return m_AgentType;
}
