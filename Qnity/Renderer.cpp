#include "QnityPCH.h"
#include "Renderer.h"
#include <SDL.h>
#include "SceneManager.h"
#include "Texture2DComponent.h"

void Qnity::Renderer::Init(SDL_Window * window)
{
	m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (m_Renderer == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateRenderer Error: ") + SDL_GetError());
	}
}

void Qnity::Renderer::Render() const
{
	SDL_RenderClear(m_Renderer);

	SceneManager::GetInstance().Render();
	
	SDL_RenderPresent(m_Renderer);
}

void Qnity::Renderer::Destroy()
{
	if (m_Renderer != nullptr)
	{
		SDL_DestroyRenderer(m_Renderer);
		m_Renderer = nullptr;
	}
}

void Qnity::Renderer::RenderTexture(SDL_Texture* texture, const float x, const float y) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	SDL_QueryTexture(texture, nullptr, nullptr, &dst.w, &dst.h);
	SDL_RenderCopy(GetSDLRenderer(), texture, nullptr, &dst);
}

void Qnity::Renderer::RenderTexture(SDL_Texture* texture, const float x, const float y, const float width, const float height) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	dst.w = static_cast<int>(width);
	dst.h = static_cast<int>(height);
	SDL_RenderCopy(GetSDLRenderer(), texture, nullptr, &dst);
}

void Qnity::Renderer::RenderTexture(SDL_Texture* texture, const SDL_Rect& src, const SDL_Rect& dst) const
{
	SDL_RenderCopy(GetSDLRenderer(), texture, &src, &dst);
}

void Qnity::Renderer::DrawRectangle(Vector2<float> point, bool filled) const
{
	SDL_Rect rect;
	rect.x = static_cast<int>(point.x);
	rect.y = static_cast<int>(point.y);
	rect.w = 32;
	rect.h = 32;

	SDL_SetRenderDrawColor(GetSDLRenderer(), 0, 255, 0, 255);
	if (filled)
	{
		SDL_RenderFillRect(GetSDLRenderer(), &rect);
	}
	else
	{
		SDL_RenderDrawRect(GetSDLRenderer(), &rect);
	}
	SDL_SetRenderDrawColor(GetSDLRenderer(), 0, 0, 0, 255);
}

void Qnity::Renderer::DrawRectangle(Rect<float> rectangle, bool filled) const
{
	SDL_Rect rect;
	rect.x = static_cast<int>(rectangle.upperLeft.x);
	rect.y = static_cast<int>(rectangle.upperLeft.y);
	rect.w = static_cast<int>(rectangle.upperRight.x - rectangle.upperLeft.x);
	rect.h = static_cast<int>(rectangle.lowerLeft.y - rectangle.upperLeft.y);

	SDL_SetRenderDrawColor(GetSDLRenderer(), 0, 255, 0, 255);
	if (filled)
	{
		SDL_RenderFillRect(GetSDLRenderer(), &rect);
	}
	else
	{
		SDL_RenderDrawRect(GetSDLRenderer(), &rect);
	}
	SDL_SetRenderDrawColor(GetSDLRenderer(), 0, 0, 0, 255);
}
