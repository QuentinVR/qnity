#pragma once

#include "BaseComponent.h"
#include <string>

struct SDL_Texture;

namespace Qnity
{
	/**
	 * Simple RAII wrapper for an SDL_Texture
	 */
	class Texture2DComponent final : public BaseComponent
	{
	public:
		void Update(int MsPerFrame) override;
		void Render() const override;
		void RenderAtPos(int x, int y) const;

		//SDL_Texture* GetSDLTexture() const;
		explicit Texture2DComponent(size_t textureNumber, bool isSprite = false);
		~Texture2DComponent();

		Texture2DComponent(const Texture2DComponent&) = delete;
		Texture2DComponent(Texture2DComponent&&) = delete;
		Texture2DComponent& operator= (const Texture2DComponent&) = delete;
		Texture2DComponent& operator= (const Texture2DComponent&&) = delete;

		void SetSpriteNumber(size_t number);		
		void SetTextureNumber(size_t number);		
		void SetColumns(size_t number);
		void SetRows(size_t number);
		void SetOffset(float x, float y);
		void SetDisplaySize(int x, int y);
		void SetHalfOffset(bool stats);

	private:

		float m_OffsetX;
		float m_OffsetY;
		int m_TextureWidth;
		int m_TextureHeight;
		int m_DisplayWidth;
		int m_DisplayHeight;
		size_t m_SpriteNumber;
		size_t m_TextureNumber;
		size_t m_Columns;
		size_t m_Rows;
		bool m_IsSprite;

		SDL_Texture* m_pTexture;

		bool m_HalfOffset;
	};
}
