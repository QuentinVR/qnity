#pragma once
#include "Windows.h"
#include <xinput.h>
#include "Singleton.h"
#include "Command.h"

namespace Qnity
{

	enum class InputType
	{
		PRESSED,
		HOLD,
		RELEASED
	};

	struct InputAction
	{
		InputAction(WORD gamepad, WORD key, InputType typeOfInput, Command* command)
			: gamepadInput{gamepad}
			, keyboardInput{key}
			, inputType{typeOfInput}
			, command{command}
			, isKeyPressed{false}
			, isPadPressed{false}
		{
		
		}

		~InputAction() { delete command; };

		WORD gamepadInput;
		WORD keyboardInput;
		InputType inputType;
		Command* command;
		bool isKeyPressed;
		bool isPadPressed;
	};

	class InputManager final : public Singleton<InputManager>
	{
	public:
		bool ProcessInput();
		bool InputState(WORD gamepad, WORD key, InputType type, bool& IsPadPressed, bool& IsKeyPressed, int PlayerNumber);

	private:
		XINPUT_STATE m_GamePadState[2];
		
		std::vector<InputAction> m_pActions;

		bool m_ErrorReported{false};
	};

}
