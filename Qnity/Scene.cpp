#include "QnityPCH.h"
#include "Scene.h"
#include "GameObject.h"
#include <algorithm>
#include "Map.h"
#include "ColliderComponent.h"

unsigned int Qnity::Scene::m_IdCounter = 0;

Qnity::Scene::Scene(const std::string& name) : m_Name(name), m_pMap{nullptr} {}

Qnity::Scene::~Scene() = default;

void Qnity::Scene::Add(const std::shared_ptr<GameObject>& object)
{
	m_Objects.push_back(object);
	object->Initialize();

	//after initialize check if there are colliders
	ColliderComponent* collider = object->GetComponent<ColliderComponent>().get();
	if (collider != nullptr)
	{
		m_pMap->AddCollider(collider);
	}
}

void Qnity::Scene::AddDuringGame(const std::shared_ptr<GameObject>& object)
{
	m_ObjectsToAdd.push_back(object);
	object->Initialize();
}

void Qnity::Scene::RootUpdate(int MsPerFrame)
{
	Update(MsPerFrame);

	for (auto& object : m_Objects)
	{
		object->Update(MsPerFrame);
	}
}

void Qnity::Scene::RootLateUpdate(int MsPerFrame)
{
	LateUpdate(MsPerFrame);

	for (auto& object : m_Objects)
	{
		object->LateUpdate(MsPerFrame);
	}

	//clean object colliders from map
	if (m_pMap != nullptr)
	{
		m_pMap->RemoveColliders();
	}

	//destroy objects that wish so
	m_Objects.erase(std::remove_if(m_Objects.begin(), m_Objects.end(), [](std::shared_ptr<GameObject> object ) {return object->IsCravingDestruction();}), m_Objects.end());

	//add objects pending to be added
	for (std::shared_ptr<GameObject> object : m_ObjectsToAdd)
	{
		Add(object);
	}

	m_ObjectsToAdd.clear();
}

void Qnity::Scene::RootRender() const
{
	Render();

	for (const auto& object : m_Objects)
	{
		object->Render();
	}
}

std::vector<std::shared_ptr<Qnity::GameObject>>& Qnity::Scene::GetObjectsInScene()
{
	return m_Objects;
}

void Qnity::Scene::RootInitialize()
{
	if (m_IsInitialized)
	{
		return;
	}
	
	//load user scene, then init all objects
	Initialize();

	for (std::shared_ptr<GameObject> object : m_Objects)
	{
		object->Initialize();
	}

	m_IsInitialized = true;
}


