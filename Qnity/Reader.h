#pragma once
#include "Singleton.h"
#include <fstream>

namespace Qnity
{
	struct Reader
	{
		std::ifstream input;

		Reader(const std::string& filename)
		{
			input.open(filename, std::ios::in);
		}

		template<typename T>
		void Read(const T& toRead)
		{
			input.read((char*)&toRead, sizeof(toRead));
		}

		void MoveReadLocation(int pos)
		{
			input.seekg(pos);
		}

		~Reader()
		{
			input.close();
		}
	};
}

