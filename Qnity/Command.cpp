#include "QnityPCH.h"
#include "Command.h"
#include "ActorBehavior.h"

Qnity::Command::Command(GameObject* owner)
	: m_pOwner{owner}
	, m_IsInitialized{false}
{

}

void Qnity::Command::RootInitialize()
{
	m_pActor = m_pOwner->GetComponent<ActorBehavior>();
	Initialize();
}
