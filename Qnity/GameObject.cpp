#include "QnityPCH.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "ActorBehavior.h"

Qnity::GameObject::GameObject()
	: m_ICraveDestruction{false}
	, m_IsInitialized{false}
	, m_HasActor{ false }
{

}

Qnity::GameObject::~GameObject()
{

}

const Qnity::Transform& Qnity::GameObject::GetTransform() const
{
	return m_Transform;
}

void Qnity::GameObject::Update(int MsPerFrame)
{
	for (std::shared_ptr<BaseComponent> component : m_pComponents)
	{
		component->Update(MsPerFrame);
	}
}

void Qnity::GameObject::LateUpdate(int MsPerFrame)
{
	for (std::shared_ptr<BaseComponent> component : m_pComponents)
	{
		component->LateUpdate(MsPerFrame);
	}
}

void Qnity::GameObject::Render() const
{
	for (std::shared_ptr<BaseComponent> component : m_pComponents)
	{
		component->Render();
	}
}

void Qnity::GameObject::AddComponent(std::shared_ptr<BaseComponent> newComponent)
{
	m_pComponents.push_back(newComponent);
	newComponent->SetOwner(this);
}

void Qnity::GameObject::SetPosition(const Vector2<float>& vec)
{
	m_Transform.SetPosition(vec.x, vec.y);
}

void Qnity::GameObject::SetPosition(float x, float y)
{
	m_Transform.SetPosition(x, y);
}

void Qnity::GameObject::Initialize()
{
	if (m_IsInitialized)
	{
		return;
	}

	for (std::shared_ptr<BaseComponent> component : m_pComponents)
	{
		component->Initialize();
	}

	m_IsInitialized = true;

	if (GetComponent<ActorBehavior>() != nullptr)
	{
		m_HasActor = true;
	}
}

bool Qnity::GameObject::IsCravingDestruction()
{
	return m_ICraveDestruction;
}

void Qnity::GameObject::SetDestroy()
{
	m_ICraveDestruction = true;

	for (std::shared_ptr<BaseComponent> component : m_pComponents)
	{
		component->SetDestroy();
	}
}

bool Qnity::GameObject::HasActor()
{
	return m_HasActor;
}
