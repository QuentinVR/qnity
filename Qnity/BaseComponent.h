#pragma once

namespace Qnity
{
	class GameObject;

	class BaseComponent
	{
	public:
		BaseComponent();
		virtual ~BaseComponent();

		BaseComponent(const BaseComponent& other) = delete;
		BaseComponent(BaseComponent&& other) = delete;
		BaseComponent& operator=(const BaseComponent& other) = delete;
		BaseComponent& operator=(BaseComponent&& other) = delete;

		virtual void Update(int MsPerFrame);
		virtual void LateUpdate(int MsPerFrame);
		virtual void Render() const;

		void SetOwner(GameObject* owner);
		virtual void Initialize();

		GameObject* GetOwner() const;

		virtual void SetDestroy();

	protected:
		//reference to object
		GameObject* m_pOwner;
		bool m_IsInitialized;
	};
}
