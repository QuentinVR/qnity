#include "QnityPCH.h"
#include "BaseComponent.h"
#include "GameObject.h"
#include "Logger.h"

Qnity::BaseComponent::BaseComponent()
	: m_pOwner{}
	, m_IsInitialized{false}
{

}

Qnity::BaseComponent::~BaseComponent()
{

}

void Qnity::BaseComponent::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}
void Qnity::BaseComponent::LateUpdate(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}


void Qnity::BaseComponent::Render() const
{

}

void Qnity::BaseComponent::SetOwner(GameObject* owner)
{
	m_pOwner = owner;
}

Qnity::GameObject* Qnity::BaseComponent::GetOwner() const
{
	return m_pOwner;
}

void Qnity::BaseComponent::Initialize()
{
	//components need to be initialized before use
	m_IsInitialized = true;
}

void Qnity::BaseComponent::SetDestroy()
{

}