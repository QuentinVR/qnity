#pragma once
#include "ColliderComponent.h"

namespace Qnity
{

	class ProjectileCollider : public ColliderComponent
	{
	public:
		ProjectileCollider(Vector2<float> velocity, ColliderTarget target, std::shared_ptr<Map> map);
		virtual ~ProjectileCollider();

		ProjectileCollider(const ProjectileCollider&) = delete;
		ProjectileCollider(ProjectileCollider&& other) = delete;
		ProjectileCollider& operator=(const ProjectileCollider& other) = delete;
		ProjectileCollider& operator=(ProjectileCollider&& other) = delete;

		virtual void Update(int MsPerFrame) override;
		virtual void LateUpdate(int MsPerFrame) override;
		virtual void Render() const override;
		virtual void Initialize() override;

		virtual void HitEvent(GameObject* other, bool terrain = false);

		void SetSpriteOffset(size_t offset);
		void SetAnimationFrames(int amount);
		void AnimationCycle(int MsPerFrame);

	protected:
		Vector2<float> m_Velocity;
		std::weak_ptr<Texture2DComponent> m_pTextureComponent;

		size_t m_SpriteOffset;
		size_t m_SpriteIncrement;
		int m_AnimationFrameTime;
		int m_AnimationTimeElapsed;
		int m_TotalAnimationSprites;

		int m_LifeTime;
		int m_TimeElapsed;
	};
}
