#pragma once
#include "BaseComponent.h"
#include "RigidBodyComponent.h"
#include "Texture2DComponent.h"
#include "InputComponent.h"

namespace Qnity
{
	enum class ActorState
	{
		idle, jumping, attacking, falling, dropDown
	};

	class ActorBehavior : public BaseComponent
	{

	public:
		ActorBehavior();
		virtual ~ActorBehavior();

		virtual void Initialize() override;
		virtual void Update(int MsPerFrame) override;
		virtual void LateUpdate(int MsPerFrame) override;
		virtual void Render() const override;

		virtual void SetState(ActorState state);

		virtual void AnimationCycle(int MsPerFrame);

		bool IsLookingRight();
		Vector2<float> GetPos(bool center);

	protected:
		std::weak_ptr<RigidBodyComponent> m_pRigidBody;
		std::weak_ptr<Texture2DComponent> m_pTextureComponent;
		std::weak_ptr<InputComponent> m_pInputComponent;

		ActorState m_State;
		bool m_StateChange;
		size_t m_SpriteOffset;
		size_t m_SpriteIncrement;
		size_t m_TotalAnimationSprites;
		size_t m_SpriteDirectionOffset;

		int m_AnimationFrameTime;
		int m_AnimationTimeElapsed;

		int m_AttackAnimationTime;
		int m_AttackAnimationTimeElapsed;

		bool m_ShowAttackAnimation;

		bool m_LookingRight;
	};
}