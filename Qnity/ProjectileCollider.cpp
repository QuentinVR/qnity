#include "QnityPCH.h"
#include "ProjectileCollider.h"


Qnity::ProjectileCollider::ProjectileCollider(Vector2<float> velocity, ColliderTarget target, std::shared_ptr<Map> map)
	: ColliderComponent{target, map}
	, m_Velocity{velocity}
	, m_pTextureComponent{}
	, m_SpriteOffset{180}
	, m_SpriteIncrement{0}
	, m_AnimationFrameTime{64}
	, m_AnimationTimeElapsed{0}
	, m_TotalAnimationSprites{8}
	, m_LifeTime{384}
	, m_TimeElapsed{}
{

}

Qnity::ProjectileCollider::~ProjectileCollider()
{

}

void Qnity::ProjectileCollider::Initialize()
{
	ColliderComponent::Initialize();
	m_pRigidBody.lock()->SetXVelocity(m_Velocity.x);
	m_pRigidBody.lock()->SetYVelocity(m_Velocity.y);

	m_pTextureComponent = m_pOwner->GetComponent<Texture2DComponent>();
}


void Qnity::ProjectileCollider::Update(int MsPerFrame)
{
	ColliderComponent::Update(MsPerFrame);

	AnimationCycle(MsPerFrame);

	m_TimeElapsed += MsPerFrame;

	//check if collision with map happened
	if (m_pRigidBody.lock()->HasHitTerrainThisFrame() || m_TimeElapsed > m_LifeTime)
	{
		HitEvent(nullptr, true);
	}
}

void Qnity::ProjectileCollider::LateUpdate(int MsPerFrame)
{
	ColliderComponent::LateUpdate(MsPerFrame);
}

void Qnity::ProjectileCollider::Render() const
{

}

void Qnity::ProjectileCollider::HitEvent(GameObject* other, bool terrain)
{
	UNREFERENCED_PARAMETER(other);
	UNREFERENCED_PARAMETER(terrain);

	m_pOwner->SetDestroy();
}


void Qnity::ProjectileCollider::SetSpriteOffset(size_t offset)
{
	m_SpriteOffset = offset;
}

void Qnity::ProjectileCollider::SetAnimationFrames(int amount)
{
	m_TotalAnimationSprites = amount;
}

void Qnity::ProjectileCollider::AnimationCycle(int MsPerFrame)
{
	m_AnimationTimeElapsed += MsPerFrame;
	if (m_AnimationFrameTime < m_AnimationTimeElapsed)
	{
		m_AnimationTimeElapsed -= m_AnimationFrameTime;
		m_SpriteIncrement++;
		m_SpriteIncrement %= m_TotalAnimationSprites;
		m_pTextureComponent.lock()->SetSpriteNumber(m_SpriteOffset + m_SpriteIncrement * 2);
	}
}


