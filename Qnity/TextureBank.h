#pragma once
#include "Singleton.h"
#include <string>
#include <map>

struct SDL_Texture;

namespace Qnity
{
	class TextureBank final : public Singleton<TextureBank>
	{
	public:

		virtual ~TextureBank();

		void SetResourcePath(const std::string& filepath);
		SDL_Texture* GetSDLTexture(int magicNumber);
		void AddSDLTexture(const std::string& filepath);

	private:
		std::string m_Resourcepath;
		std::vector<SDL_Texture*> m_pTextures;
	};
}
