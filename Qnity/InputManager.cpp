#include "QnityPCH.h"
#include "InputManager.h"
#include <SDL.h>
#include "Logger.h"

bool Qnity::InputManager::ProcessInput()
{
	ZeroMemory(&m_GamePadState[0], sizeof(XINPUT_STATE));
	DWORD dwResult = XInputGetState(0, &m_GamePadState[0]);

	if (!m_ErrorReported && dwResult != ERROR_SUCCESS)
	{
		Logger().LogWarning("Controller player 1 not connected");
		m_ErrorReported = true;
	}

	ZeroMemory(&m_GamePadState[1], sizeof(XINPUT_STATE));
	dwResult = XInputGetState(1, &m_GamePadState[1]);

	if (!m_ErrorReported && dwResult != ERROR_SUCCESS)
	{
		Logger().LogWarning("Controller player 2 not connected");
		m_ErrorReported = true;
	}

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) {
			return false;
		}
		if (e.type == SDL_KEYDOWN) {
			
		}
		if (e.type == SDL_MOUSEBUTTONDOWN) {
			
		}
	}

	return true;
}

bool Qnity::InputManager::InputState(WORD gamepad, WORD key, InputType type, bool& isPadPressed, bool& isKeyPressed, int PlayerNumber)
{

	bool actionState{false};
	//check gamepad buttons
	if (gamepad != 0)
	{
		switch (type)
		{
		case Qnity::InputType::PRESSED:
			if ((m_GamePadState[PlayerNumber].Gamepad.wButtons & gamepad)
				&& !isPadPressed)
			{
				actionState = true;
			}
			break;
		case Qnity::InputType::HOLD:
			if ((m_GamePadState[PlayerNumber].Gamepad.wButtons & gamepad)
				&& isPadPressed)
			{
				actionState = true;
			}
			break;
		case Qnity::InputType::RELEASED:
			if (!(m_GamePadState[PlayerNumber].Gamepad.wButtons & gamepad)
				&& isPadPressed)
			{
				actionState = true;
			}
			break;
		default:
			break;
		}

		if (m_GamePadState[PlayerNumber].Gamepad.wButtons & gamepad)
		{
			isPadPressed = true;
		}
		else
		{
			isPadPressed = false;
		}
	}

	if (key != 0)
	{

		bool keyState = (GetAsyncKeyState(key) & 0x8000);

		switch (type)
		{
		case Qnity::InputType::PRESSED:
			if (keyState && !isKeyPressed)
			{
				actionState = true;
			}
			break;
		case Qnity::InputType::HOLD:
			if (keyState && isKeyPressed)
			{
				actionState = true;
			}
			break;
		case Qnity::InputType::RELEASED:
			if (keyState && isKeyPressed)
			{
				actionState = true;
			}
			break;
		default:
			break;
		}

		if (keyState)
		{
			isKeyPressed = true;
		}
		else
		{
			isKeyPressed = false;
		}
	}

	return actionState;
}
