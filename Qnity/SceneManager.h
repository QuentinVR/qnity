#pragma once
#include "Singleton.h"
#include <string>

namespace Qnity
{
	class Scene;
	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		Scene& AddScene(std::shared_ptr<Scene> newScene);

		void Update(int MsPerFrame);
		void LateUpdate(int MsPerFrame);
		void Render();

		Scene& GetCurrentScene();
		void GoToNextLevel();

	private:
		friend class Singleton<SceneManager>;
		SceneManager() = default;
		std::vector<std::shared_ptr<Scene>> m_Scenes;
		size_t m_IndexCurrentScene{};
	};
}
