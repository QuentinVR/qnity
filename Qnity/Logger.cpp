#include "Logger.h"

#ifdef _DEBUG
Qnity::LogLevel Qnity::Logger::s_SeverityLevel = Qnity::LogLevel::NOLOGGING;
#else
Qnity::LogLevel Qnity::Logger::s_SeverityLevel = Qnity::LogLevel::INFO;
#endif
bool Qnity::Logger::s_IsInitialized = false;
bool Qnity::Logger::s_ErrorsDetected = false;

std::mutex Qnity::Logger::write_mutex;

Qnity::Logger::Logger()
{

}

Qnity::Logger::~Logger()
{

}

void Qnity::Logger::Initialize()
{
	//delete content of file
	output.open(filename, std::ofstream::out | std::ofstream::trunc);
	output.close();
	s_IsInitialized = true;
}

Qnity::LogLevel& Qnity::Logger::GetReportingLevel()
{
	return s_SeverityLevel;
}

void Qnity::Logger::Log(const std::string& message, Qnity::LogLevel level)
{
	if (!s_IsInitialized)
	{
		throw std::runtime_error("Logger was not initialized properly");
	}

	if (GetReportingLevel() > level)
	{
		return;
	}
	//prevent multiple logs happening at the same time
	const std::lock_guard<std::mutex> lock(write_mutex);
	output.open(filename, std::ios::app);

	switch (level)
	{
	case Qnity::LogLevel::INFO:
		output << "INFO:	";
		break;
	case Qnity::LogLevel::DEBUG:
		output << "DEBUG:	";
		break;
	case Qnity::LogLevel::WARNING:
		output << "WARNING:	";
		break;
	case Qnity::LogLevel::logERROR:
		output << "ERROR:	";
		s_ErrorsDetected = true;
		break;
	default:
		break;
	}

	//output message
	output << message;
	output << std::endl;
	output.close();
}

void Qnity::Logger::ReportErrors()
{
	if (s_ErrorsDetected)
	{
		std::cout << std::endl;
		std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
		std::cout << "One or more ERROR messages were logged." << std::endl;
		std::cout << "*sad Dev noises*" << std::endl;
	}
	else
	{
		std::cout << std::endl;
		std::cout << "No ERROR messages, good work, you stud!" << std::endl;
		std::cout << "Who's the man? You're the man!" << std::endl;
	}
}