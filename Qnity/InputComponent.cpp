#include "QnityPCH.h"
#include "InputComponent.h"
#include "Logger.h"
#include "ActorBehavior.h"

Qnity::InputComponent::InputComponent()
	: m_pCurrentCommands{}
	, m_pNextCommands{}

{

}

Qnity::InputComponent::~InputComponent()
{
	m_pCurrentCommands.clear();
	m_pNextCommands.clear();
}

void Qnity::InputComponent::Initialize()
{
	BaseComponent::Initialize();
	m_pActor = m_pOwner->GetComponent<ActorBehavior>();
}

void Qnity::InputComponent::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void Qnity::InputComponent::LateUpdate(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
	m_pCurrentCommands = m_pNextCommands;
	m_pNextCommands.clear();
}

void Qnity::InputComponent::Render() const
{

}

std::vector<Qnity::Command*> Qnity::InputComponent::GetCurrentCommands() const
{
	return m_pCurrentCommands;
}