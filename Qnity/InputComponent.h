#pragma once
#include "InputManager.h"
#include "BaseComponent.h"

namespace Qnity
{
	class ActorBehavior;

	class InputComponent : public BaseComponent
	{
	public:
		InputComponent();
		virtual ~InputComponent();

		InputComponent(const InputComponent& other) = delete;
		InputComponent(InputComponent&& other) = delete;
		InputComponent& operator=(const InputComponent& other) = delete;
		InputComponent& operator=(InputComponent&& other) = delete;

		virtual void Update(int MsPerFrame) override;
		virtual void LateUpdate(int MsPerFrame) override;
		virtual void Render() const override;
		virtual void Initialize() override;

		std::vector<Command*> GetCurrentCommands() const;

	protected:
		std::vector<Command*> m_pCurrentCommands;
		std::vector<Command*> m_pNextCommands;

		std::weak_ptr<ActorBehavior> m_pActor;
	};
}