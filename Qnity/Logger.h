#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <mutex>

//based on www.drdobbs.com/cpp/logging-in-c/201804215
//based on www.drdobbs.com/cpp/leightweight-logger-for-c/240147505
namespace Qnity
{
	enum class LogLevel { INFO, DEBUG, WARNING, logERROR, NOLOGGING};

	class Logger
	{
	public:
		Logger();
		virtual ~Logger();
		void Initialize();
		void Log(const std::string& message, LogLevel level = LogLevel::DEBUG);
		inline void LogError(const std::string& message) { Log(message, LogLevel::logERROR); };
		inline void LogWarning(const std::string& message) { Log(message, LogLevel::WARNING); };
		inline void LogINFO(const std::string& message) { Log(message, LogLevel::INFO); };

		static LogLevel& GetReportingLevel();

		static std::mutex write_mutex;

		void ReportErrors();

	protected:
		std::ofstream output;
		const std::string filename = "log.txt";

	private:
		static LogLevel s_SeverityLevel;
		static bool s_IsInitialized;
		static bool s_ErrorsDetected;
	};
}