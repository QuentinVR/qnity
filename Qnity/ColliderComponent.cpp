#include "QnityPCH.h"
#include "ColliderComponent.h"
#include "Renderer.h"
#include "ActorBehavior.h"

Qnity::ColliderComponent::ColliderComponent(ColliderTarget target, std::shared_ptr<Map> map)
	: m_pRigidBody{}
	, m_Target{target}
	, m_pMap{map}
{

}

Qnity::ColliderComponent::~ColliderComponent()
{

}

void Qnity::ColliderComponent::Initialize()
{
	BaseComponent::Initialize();
	m_pRigidBody = m_pOwner->GetComponent<RigidBodyComponent>();
}

void Qnity::ColliderComponent::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void Qnity::ColliderComponent::LateUpdate(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void Qnity::ColliderComponent::Render() const
{
	Renderer::GetInstance().DrawRectangle(m_pRigidBody.lock()->GetBoundingBox(true));
}

void Qnity::ColliderComponent::HitEvent(GameObject* other, bool terrain)
{
	UNREFERENCED_PARAMETER(other);
	UNREFERENCED_PARAMETER(terrain);
}

Qnity::Rect<float> Qnity::ColliderComponent::GetBoundingBox() const
{
	return m_pRigidBody.lock()->GetBoundingBox(true);
}


Qnity::ColliderTarget Qnity::ColliderComponent::GetTarget() const
{
	return m_Target;
}

void Qnity::ColliderComponent::SetDestroy()
{
	m_pMap->MarkForRemoval(this);
}




