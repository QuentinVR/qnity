#include "QnityPCH.h"
#include "DemoScene.h"
#include "SceneManager.h"
#include "Texture2DComponent.h"
#include "TextComponent.h"
#include "GameObject.h"
#include "Scene.h"
#include "FPSCounter.h"
#include "ResourceManager.h"

DemoScene::DemoScene(const std::string& name)
	: Scene{name}
{

}

void DemoScene::Update(int MsPerFrame)
{

	UNREFERENCED_PARAMETER(MsPerFrame);
}

void DemoScene::LateUpdate()
{

}

void  DemoScene::Render() const
{
}

void DemoScene::Initialize()
{
	using namespace Qnity;
	//create background
	std::shared_ptr<GameObject> background = std::make_shared<GameObject>();
	std::shared_ptr<Texture2DComponent> textureBackground = std::make_shared<Texture2DComponent>("../Resources/background.jpg");
	background->AddComponent(textureBackground);

	Add(background);

	//add logo
	std::shared_ptr<GameObject> logo = std::make_shared<GameObject>();
	std::shared_ptr<Texture2DComponent> textureLogo = std::make_shared<Texture2DComponent>("../Resources/logo.png");
	logo->SetPosition(216.f, 180.f);
	logo->AddComponent(textureLogo);

	Add(logo);

	auto font = ResourceManager::GetInstance().LoadFont("../Resources/Lingua.otf", 36);
	std::shared_ptr<GameObject> textObject = std::make_shared<GameObject>();
	auto textComponent = std::make_shared<TextComponent>("Programming 4 Assignment", font);
	textObject->SetPosition(80.f, 20.f);
	textObject->AddComponent(textComponent);
	Add(textObject);

	//add fps counter
	std::shared_ptr<TextComponent> textfps = std::make_shared<TextComponent>("0", font);
	std::shared_ptr<FPSCounter> fpsCounter = std::make_shared<FPSCounter>();
	std::shared_ptr<GameObject> fpsCounterObject = std::make_shared<GameObject>();

	fpsCounterObject->SetPosition(10.f, 440.f);
	fpsCounterObject->AddComponent(textfps);
	fpsCounterObject->AddComponent(fpsCounter);
	Add(fpsCounterObject);
}