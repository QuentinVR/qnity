#include "QnityPCH.h"
#include "TestScene.h"
#include "PlayerInputComponent.h"
#include "JumpCommand.h"
#include "MoveCommand.h"
#include "DropDownCommand.h"
#include "ShootCommand.h"
#include "FPSCounter.h"
#include <Texture2DComponent.h>
#include "RigidBodyComponent.h"
#include "PlayerBehavior.h"
#include "TextureBank.h"
#include "ZenChanInput.h"
#include "ZenChanBehavior.h"
#include <ColliderComponent.h>
#include "EnemyCollider.h"


TestScene::TestScene(const std::string& name)
	: Scene{name}
{

}

void TestScene::Update(int MsPerFrame)
{
	UNREFERENCED_PARAMETER(MsPerFrame);
}

void TestScene::LateUpdate()
{

}

void TestScene::Render() const
{
	m_pMap->Render();
}

void TestScene::Initialize()
{
	using namespace Qnity;

	//load all textures
	TextureBank::GetInstance().SetResourcePath("../Resources/");
	TextureBank::GetInstance().AddSDLTexture("blocks.png");
	TextureBank::GetInstance().AddSDLTexture("sprites0.png");
	TextureBank::GetInstance().AddSDLTexture("sprites1.png");
	TextureBank::GetInstance().AddSDLTexture("sprites2.png");
	TextureBank::GetInstance().AddSDLTexture("sprites3.png");
	TextureBank::GetInstance().AddSDLTexture("sprites4.png");
	TextureBank::GetInstance().AddSDLTexture("sprites5.png");


	m_pPlayerCharacter = std::make_shared<GameObject>();

	std::shared_ptr<PlayerInputComponent> pInput = std::make_shared<PlayerInputComponent>();
	m_pPlayerCharacter->AddComponent(pInput);

	std::shared_ptr<Texture2DComponent> pTexture = std::make_shared<Texture2DComponent>(int(TextureNames::sprites0), true);
	pTexture->SetSpriteNumber(0);
	pTexture->SetDisplaySize(64, 64);
	m_pPlayerCharacter->AddComponent(pTexture);
	m_pPlayerCharacter->SetPosition(100.f, 500.f);

	m_pMap = std::make_shared<Map>(this);
	m_pMap->LoadTexture(int(TextureNames::blocks), 0);
	m_pMap->LoadTiles("../Resources/leveldata.dat", 0);

	std::shared_ptr<RigidBodyComponent> rb = std::make_shared<RigidBodyComponent>(m_pMap);
	m_pPlayerCharacter->AddComponent(rb);
	rb->SetColliderGroup(ColliderTarget::goodGuys);

	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_A, VK_UP, InputType::PRESSED, new JumpCommand{m_pPlayerCharacter.get()} });
	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_LEFT, VK_LEFT, InputType::HOLD, new MoveCommand{m_pPlayerCharacter.get(), -256.f} });
	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_RIGHT, VK_RIGHT, InputType::HOLD, new MoveCommand{m_pPlayerCharacter.get(), 256} });
	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_DPAD_DOWN, VK_DOWN, InputType::HOLD, new DropDownCommand{m_pPlayerCharacter.get()} });
	pInput->RegisterInput(new InputAction{ XINPUT_GAMEPAD_X, VK_SPACE, InputType::PRESSED, new ShootCommand{m_pPlayerCharacter.get(), m_pMap, 0, m_pPlayerCharacter} });

	std::shared_ptr<PlayerBehavior> playerBehavior = std::make_shared<PlayerBehavior>();
	m_pPlayerCharacter->AddComponent(playerBehavior);

	//spawn zen chan

	std::shared_ptr<GameObject> zenchanObject = std::make_shared<GameObject>();
	std::shared_ptr<Texture2DComponent> pTextureZenChan = std::make_shared<Texture2DComponent>(int(TextureNames::sprites0), true);
	pTextureZenChan->SetDisplaySize(64, 64);
	std::shared_ptr<ZenChanBehavior> pZenChanBehavior = std::make_shared<ZenChanBehavior>();
	std::shared_ptr<ZenChanInput> pZenChanInput = std::make_shared<ZenChanInput>(m_pPlayerCharacter);
	std::shared_ptr<RigidBodyComponent> rbZenChan = std::make_shared<RigidBodyComponent>(m_pMap);
	rbZenChan->SetColliderGroup(Qnity::ColliderTarget::badGuys);
	std::shared_ptr<EnemyCollider> pEnemyCollider = std::make_shared<EnemyCollider>(m_pMap);

	zenchanObject->SetPosition(400.f, 64.f);
	zenchanObject->AddComponent(pTextureZenChan);
	zenchanObject->AddComponent(pZenChanBehavior);
	zenchanObject->AddComponent(pZenChanInput);
	zenchanObject->AddComponent(rbZenChan);
	zenchanObject->AddComponent(pEnemyCollider);

	Add(m_pPlayerCharacter);
	Add(zenchanObject);
}