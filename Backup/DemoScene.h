#pragma once
#include "Scene.h"
#include "SceneManager.h"

class DemoScene : public Qnity::Scene
{

public:

	DemoScene(const std::string& name);
	virtual ~DemoScene() = default;
	DemoScene(const DemoScene& other) = delete;
	DemoScene(DemoScene&& other) = delete;
	DemoScene& operator=(const DemoScene& other) = delete;
	DemoScene& operator=(DemoScene&& other) = delete;

	void Update(int MsPerFrame) override;
	void LateUpdate() override;
	void Render() const override;

	void Initialize() override;

};