#pragma once
#include "Scene.h"
#include "SceneManager.h"
#include <GameObject.h>
#include <Map.h>

enum class TextureNames
{
	blocks = 0,
	sprites0 = 1,
	sprites1 = 2,
	sprites2 = 3,
	sprites3 = 4,
	sprites4 = 5,
	sprites5 = 6,
};

class TestScene : public Qnity::Scene
{

public:

	TestScene(const std::string& name);
	virtual ~TestScene() = default;
	TestScene(const TestScene& other) = delete;
	TestScene(TestScene&& other) = delete;
	TestScene& operator=(const TestScene& other) = delete;
	TestScene& operator=(TestScene&& other) = delete;

	void Update(int MsPerFrame) override;
	void LateUpdate() override;
	void Render() const override;

	void Initialize() override;

private:

	std::shared_ptr<Qnity::GameObject> m_pPlayerCharacter;
};